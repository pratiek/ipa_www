import React from "react";
import { Route, Link, BrowserRouter as Router } from 'react-router-dom'

// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import "assets/css/material-dashboard-react.css?v=1.6.0";
import Typography from '@material-ui/core/Typography';
import InputAdornment from "@material-ui/core/InputAdornment";
import Person from "@material-ui/icons/Person";
import Particles from "react-particles-js";
import logo from "assets/img/reactlogo.png"
import Snackbar from "components/Snackbar/Snackbar.jsx";
import AddAlert from "@material-ui/icons/AddAlert";
import { Redirect } from 'react-router';
import IconButton from '@material-ui/core/IconButton';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import Texty from 'rc-texty';
import 'rc-texty/assets/index.css';
import Context from "Context.js";
const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "5px",
    textDecoration: "none",
  },
  loginForm: {
    marginTop: "50px",
    marginLeft: "33%",
    position: 'relative',
    alignItems: 'center',
    justifyContent: 'center',
    align: 'center',
    margin: 0,
    padding: 0
  },
  formFields: {
    width: '100%',
    marginTop: '20px'
  },
  logo: {
    'margin-left': '35%'
  },
  button: {
    marginTop: '10px',
    marginBottom: '15px'
  },
  loginPage: {
    margin: '0px 0px 0px 0px'
  },
  particle: {
      position: 'fixed',
      right: '0',
      right: '0',
      bottom: '0',
      left: '0',
      'z-index': '-1'
    }
};

const Services = require('services/RemoteServices.jsx');

class Login extends React.Component {
  constructor(props) {
    super(props)
    this.sendLogin = this.sendLogin.bind(this)
    this.state = {
      loginDetails: {
        username: '',
        password: ''
      },
      loading: false,
      snack: false,
      snackMessage: '',
      toDashboard: false,
      showPassword: false,
      show: true
      };
  }

  componentDidMount(){
    document.body.style.background = "#4a6a8e"
  }

  onEnter() {
    return {
      enter: [
        {
          scale: 2,
          opacity: 0,
          type: 'set',
        },
        { scale: 1.2, opacity: 1, duration: 100 },
        { scale: 0.9, duration: 200 },
        { scale: 1.05, duration: 150 },
        { scale: 1, duration: 100 },
      ],
      leave: {
        opacity: 0, scale: 0,
      },
    };
  }

  handleChange = name => event => {
    const loginDetails = Object.assign({}, this.state.loginDetails, {  [name]: event.target.value });
    this.setState({ loginDetails });
  };

  handleClickShowPassword = () => {
    this.setState(state => ({ showPassword: !state.showPassword }));
  };

  sendLogin () {
    this.setState({ loading: true})
    Services.sendLogin(this.state.loginDetails).then(function(response){
      if(!response.success) this.setState({ snack: true, snackMessage: response.message, loading: false });
      if(response.token) {
      localStorage.setItem('token', response.token)
      var userDetails = this.state.loginDetails.username
      localStorage.setItem('userDetails', userDetails);
      localStorage.setItem('firstLogin', true);
      this.setState({ toDashboard: true, loading: false });
      }
    }.bind(this))
    setTimeout(function(){
      this.setState({snack: false});
    }.bind(this),6000);
  };

  render () {
    const particlesOptions = {
      "particles": {
        "number": {
          "value": 80,
          "density": {
            "enable": true,
            "value_area": 800
          }
        },
        "color": {
          "value": "#ffffff"
        },
        "shape": {
          "type": "circle",
          "stroke": {
            "width": 0,
            "color": "#000000"
          },
          "polygon": {
            "nb_sides": 5
          },
          "image": {
            "src": "img/github.svg",
            "width": 100,
            "height": 100
          }
        },
        "opacity": {
          "value": 0.5,
          "random": false,
          "anim": {
            "enable": false,
            "speed": 1,
            "opacity_min": 0.1,
            "sync": false
          }
        },
        "size": {
          "value": 2,
          "random": true,
          "anim": {
            "enable": false,
            "speed": 40,
            "size_min": 0.1,
            "sync": false
          }
        },
        "line_linked": {
          "enable": true,
          "distance": 150,
          "color": "#ffffff",
          "opacity": 0.4,
          "width": 1
        },
        "move": {
          "enable": true,
          "speed": 2,
          "direction": "none",
          "random": false,
          "straight": false,
          "out_mode": "out",
          "bounce": false,
          "attract": {
            "enable": false,
            "rotateX": 600,
            "rotateY": 1200
          }
        }
      },
      "interactivity": {
        "detect_on": "canvas",
        "events": {
          "onhover": {
            "enable": true,
            "mode": "grab"
          },
          "onclick": {
            "enable": true,
            "mode": "push"
          },
          "resize": true
        },
        "modes": {
          "grab": {
            "distance": 150,
            "line_linked": {
              "opacity": 1
            }
          },
          "bubble": {
            "distance": 400,
            "size": 40,
            "duration": 2,
            "opacity": 8,
            "speed": 3
          },
          "repulse": {
            "distance": 200,
            "duration": 0.4
          },
          "push": {
            "particles_nb": 4
          },
          "remove": {
            "particles_nb": 2
          }
        }
      },
      "retina_detect": true,
      "color": {
        "value": ["#BD10E0","#B8E986","#50E3C2","#FFD300","#E86363"]
      }
    };
    const { classes } = this.props;
    if (this.state.toDashboard === true) {
      return <Redirect to='/admin/dashboard' />
    }
    return (
      <div>
        <Particles className={classes.particle} params={particlesOptions} />
      <Context.Consumer>
         
        {({ username }) => (
      <div className={classes.loginForm}>
        { username }
          <GridItem xs={12} sm={12} md={6} >
          <img src={logo} alt="Logo"  height="100" className={classes.logo}/>
            <Card>
              <CardHeader color="primary">
                <h5 align="center">
                  <div className="texty-demo">
                    <Texty enter={this.onEnter}>{this.state.show && 'INVENTORY PROCESS AUTOMATION'}</Texty>
                  </div>
                </h5>
              </CardHeader>
              <CardBody>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                  <FormControl fullWidth className={classes.formFields}>
                    <InputLabel htmlFor="adornment-username">Username</InputLabel>
                    <Input
                      id="adornment-username"
                      value={this.state.amount}
                      onChange={this.handleChange('username')}
                      endAdornment={<InputAdornment position="end"><Person></Person></InputAdornment>}
                      onKeyPress={event => {
                        if (event.key === 'Enter') {
                          this.sendLogin()
                        }
                      }}
                    />
                </FormControl>
                  </GridItem>
                  </GridContainer>
                  <GridContainer>
                  <GridItem xs={12} sm={12} md={12}>
                      <FormControl fullWidth className={classes.formFields}>
                        <InputLabel htmlFor="adornment-password">Password</InputLabel>
                        <Input
                          id="adornment-password"
                          type={this.state.showPassword ? 'text' : 'password'}
                          value={this.state.password}
                          onChange={this.handleChange('password')}
                          endAdornment={
                            <InputAdornment position="end">
                              <IconButton
                                aria-label="Toggle password visibility"
                                onClick={this.handleClickShowPassword}
                              >
                                {this.state.showPassword ? <Visibility /> : <VisibilityOff />}
                              </IconButton>
                            </InputAdornment>
                          }
                          onKeyPress={event => {
                            if (event.key === 'Enter') {
                              this.sendLogin()
                            }
                          }}
                        />
                    </FormControl>
                  </GridItem>
                </GridContainer>
              </CardBody>
              <CardFooter>
                <Button color="primary" onClick={this.sendLogin} className={classes.button}>
                  {this.state.loading ? 'Logging in...' : 'Log In'}
                </Button>
              </CardFooter>
            </Card>
            
            <Typography variant="caption" display="block" gutterBottom>
            <p align="center" variant="caption">Developed by ITNepal Pvt. Ltd.</p>
            </Typography>
          </GridItem>
          {this.state.snack &&
          <Snackbar
                    place="br"
                    color="danger"
                    icon={AddAlert}
                    message={this.state.snackMessage}
                    open={this.state.snack}
                    closeNotification={() => this.setState({snack:false})}
                    close
                />
      }
      </div>
      )}
      </Context.Consumer>
      </div>
    );
  }
}

Login.contextType = Context;

export default withStyles(styles)(Login);