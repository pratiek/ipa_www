import React from "react";
import CustomInput from "components/CustomInput/CustomInput.jsx";


class Storage extends React.Component {
    //state = {}
    constructor(props){
      super(props);
      this.state = {
        values : []
      }
    }
    handleChange(value){
        console.log(value);
        this.state.values.push(value);
    }

    render () {
        return (
            <div>
              <p>Please scan your qr code</p>
              <CustomInput
                    autofocus
                    labelText="QR Codes"
                    id="qr-codes"
                    formControlProps={{
                      fullWidth: true
                    }}
                    inputProps={{
                      multiline: true,
                      rows: 5
                    }}
                   
                    onChange={this.handleChange}
                  />

                {this.state.values}
            </div>
          );
      }
}

export default Storage;
