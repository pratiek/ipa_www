import React from "react";
import PropTypes from "prop-types";
// react plugin for creating charts
import ChartistGraph from "react-chartist";
// @material-ui/core
import withStyles from "@material-ui/core/styles/withStyles";
import Icon from "@material-ui/core/Icon";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardIcon from "components/Card/CardIcon.jsx";
import CardBody from "components/Card/CardBody.jsx";
import Snackbar from "components/Snackbar/Snackbar.jsx";
import AddAlert from "@material-ui/icons/AddAlert";
import Context from "Context.js";

import {
  dailySalesChart,
  completedTasksChart
} from "variables/charts.jsx";

import dashboardStyle from "assets/jss/material-dashboard-react/views/dashboardStyle.jsx";


const Services = require('services/RemoteServices.jsx');

class Dashboard extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      value: 0,
    snackBar: localStorage.getItem('firstLogin'),
    stats: {
      locationOccupied: null,
      barcodesStored: null,
      barcodesShipped: null,
      locationFreed: null,
      ItemsMoved: []
    },
    itemMovementData: null,
    itemMovementLabels: null,
    itemMovementOutData: null,
    itemMovementOutLabels: null
    }
  }

  componentDidMount(){
    document.body.style.background = "gainsboro"
    var firstLogin = localStorage.getItem('firstLogin')
    this.setState({ snackBar: firstLogin })
    if(firstLogin) {
      setTimeout(function(){
        localStorage.setItem('firstLogin', false);
        this.setState({snackBar: false})
      }.bind(this),10000);
    }
    Services.getDashboardStats().then((response) => {
      this.setState({stats: response})
      var itemsMoved = this.state.stats.ItemsMoved;
      var xAxisData = [], yAxisData = [];
      for(var item in itemsMoved) {
        xAxisData.push(itemsMoved[item].dateRange)
        yAxisData.push(itemsMoved[item].movementCount)
      }

      var itemsMovedOut = this.state.stats.ItemsMovedOut;
      var xAxisOutData = [], yAxisOutData = [];
      for(var item in itemsMovedOut) {
        xAxisOutData.push(itemsMovedOut[item].dateRange)
        yAxisOutData.push(itemsMovedOut[item].movementCount)
      }

      this.setState({itemMovementData:yAxisData, itemMovementLabels:xAxisData, itemMovementOutLabels:xAxisOutData , itemMovementOutData: yAxisOutData})
    })
  }

  handleChange = (event, value) => {
    this.setState({ value });
  };

  handleChangeIndex = index => {
    this.setState({ value: index });
  };
  render() {
    const { classes } = this.props;
    var movementData = {
      labels: this.state.itemMovementLabels,
      series: [this.state.itemMovementData]
    }
    var movementOutData = {
      labels: this.state.itemMovementOutLabels,
      series: [this.state.itemMovementOutData]
    }
    const firstLoginValue = localStorage.getItem('firstLogin')
    return (
      <div>
        {
          (firstLoginValue == 'true') &&
          <Snackbar
            place="br"
            color="success"
            icon={AddAlert}
            message="Welcome, Logged in successfully!"
            open={this.state.snackBar}
            closeNotification={() => this.setState({snackBar:false})}
            close
          />
        }
        <GridContainer>
          <GridItem xs={12} sm={6} md={3}>
            <Card>
              <CardHeader color="warning" stats icon>
                <CardIcon color="success">
                  <Icon>store</Icon>
                </CardIcon>
                <p className={classes.cardCategory}>Barcode Stored</p>
                <h3 className={classes.cardTitle}>
                  {this.state.stats.barcodesStored}
                </h3>
              </CardHeader>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card>
              <CardHeader color="success" stats icon>
                <CardIcon color="danger">
                 <Icon>local_shipping</Icon>
                </CardIcon>
                <p className={classes.cardCategory}>Barcode Shipped Out</p>
                <h3 className={classes.cardTitle}>{this.state.stats.barcodesShipped}</h3>
              </CardHeader>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card>
              <CardHeader color="danger" stats icon>
                <CardIcon color="success">
                  <Icon>add_location</Icon>
                </CardIcon>
                <p className={classes.cardCategory}>Location Occupied</p>
                <h3 className={classes.cardTitle}>{this.state.stats.locationOccupied}</h3>
              </CardHeader>
            </Card>
          </GridItem>
          <GridItem xs={12} sm={6} md={3}>
            <Card>
              <CardHeader color="info" stats icon>
                <CardIcon color="danger">
                  <Icon>location_off</Icon>
                </CardIcon>
                <p className={classes.cardCategory}>Location Freed</p>
                <h3 className={classes.cardTitle}>{this.state.stats.locationFreed}</h3>
              </CardHeader>
            </Card>
          </GridItem>
        </GridContainer>
        <GridContainer>
        { movementData.labels &&
        <GridItem xs={12} sm={12} md={6}>
        <Card chart>
          <CardHeader color="success">
            <ChartistGraph
              className="ct-chart"
              data={movementData}
              type="Line"
              options={dailySalesChart.options}
              listener={dailySalesChart.animation}
            />
          </CardHeader>
          <CardBody>
            <h4 className={classes.cardTitle}>Weekly Performance IN</h4>
            <p className={classes.cardCategory}>
              Performance by Week.
            </p>
          </CardBody>
        </Card>
      </GridItem>
      }
          
          { movementOutData.labels &&
          <GridItem xs={12} sm={12} md={6}>
            <Card chart>
              <CardHeader color="danger">
                <ChartistGraph
                  className="ct-chart"
                  data={movementOutData}
                  type="Line"
                  options={completedTasksChart.options}
                  listener={completedTasksChart.animation}
                />
              </CardHeader>
              <CardBody>
                <h4 className={classes.cardTitle}>Weekly Performance Out</h4>
                <p className={classes.cardCategory}>
                Performance by Week.
                </p>
              </CardBody>
            </Card>
          </GridItem>
          }
        </GridContainer>
      </div>
    );
  }
}

Dashboard.contextType = Context;

Dashboard.propTypes = {
  classes: PropTypes.object.isRequired
};

export default (withStyles(dashboardStyle)(Dashboard));
