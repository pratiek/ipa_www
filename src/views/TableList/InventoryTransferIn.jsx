import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import MaterialTable from 'material-table'
import Dialog from '@material-ui/core/Dialog';
import DialogPrint from "views/Dialog/Dialog.jsx"
import Snackbar from "components/Snackbar/Snackbar.jsx";
import AddAlert from "@material-ui/icons/AddAlert";
import ScanDialog from "views/Dialog/ScanDialog.jsx"
import ViewBarcode from "views/Dialog/ViewBarcode.jsx"
import CircularProgress from '@material-ui/core/CircularProgress';
import Fab from '@material-ui/core/Fab';
import Icon from '@material-ui/core/Icon';
import ReleasedItemsDialog from "views/Dialog/ReleasedItemsDialog.jsx"
import Context from "Context.js";
const Services = require('services/RemoteServices.jsx');
const utils = require('../../utils/utils')
const userRoles = require('../../consts/userRoles');


const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  },
  dialogBox: {
    minWidth: '500px'
  },
  itemTable: {
    padding: "0px 5px 0px 5px"
  }
};

class InventoryTransferIn extends React.Component {
  constructor(props) {
    super(props)
    this.handleClose = this.handleClose.bind(this)
    this.handlePrintSuccess = this.handlePrintSuccess.bind(this)
    this.state = {
      printDialogOpen: false,
      scanDialog: false,
      itemCode: null,
      itemDescription: '', 
      quantity: null,
      documentIdentifier: null,
      successSnack: false,
      selectedRow: null,
      documentDate: '',
      Data: [],
      ItemData: [],
      loading: true,
      viewBarcodeDialog: false,
      printCount: null
    };
  }

  handleClickOpen = (itemCode, itemDesc, quantity, count) => {
    this.setState({ printDialogOpen: true, itemCode: itemCode, itemDescription: itemDesc, quantity: quantity, printCount: count });
  };

  handleClose() {
    this.setState({ printDialogOpen: false, scanDialog: false, viewBarcodeDialog: false });
  };

  handlePrintSuccess() {
    this.setState({ printDialogOpen: false, successSnack: true, scanDialog: false });
    setTimeout(function(){
      this.setState({successSnack: false});
    }.bind(this),6000);
  };

      
  setDate (DocumentDate) {
    var month = DocumentDate.substr(0, DocumentDate.indexOf('/'))
    var year = DocumentDate.substr(DocumentDate.lastIndexOf("/") +3, 3)
    if(month.length == 1) {
      month = '0'+ month
    }
    const finalDate = month + year
    this.setState({ documentDate: finalDate });
  };

  getItemData (documentIdentifier, rowIdx, documentDate) {
   this.setDate(documentDate)
    this.setState({ documentIdentifier: documentIdentifier });
    if(this.state.Data[rowIdx]['items'] && this.state.Data[rowIdx]['items'].length > 0)
        return;

    Services.getInventoryTransferInItems(documentIdentifier).then(function(grnItems){
      this.setState({ ItemData: grnItems });
      var newData = this.state.Data;
      newData[rowIdx]['items'] = grnItems;
      this.setState({Data : newData});
    }.bind(this))
  };

  componentDidMount() {
    Services.getInventoryTransferIn().then(function(grn){
      this.setState({ Data: grn, loading: false });
    }.bind(this))
  }

  scanBarCode(rowData) {
    this.setState({ scanDialog: true, documentIdentifier: rowData.DocumentIdentifier });
  };

  setBarcodeViewDialog (rowData) {
    this.setState({viewBarcodeDialog: true, itemCode: rowData.Code, storedData: rowData.ReleasedItems})
  }

  render () {
    const { classes } = this.props;
    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
        { this.state.loading && 
          <div align="center">
            <CircularProgress
                  /><br />
                  Loading... Please wait
          </div>
                }
                { !(this.state.loading) && 
                    <MaterialTable
                    columns={[
                      { title: 'Voucher Number', field: 'DocumentIdentifier.VoucherNo', render: rowData => <b>{rowData.DocumentIdentifier.VoucherNo}</b> },
                      { title: 'Location Name', field: 'DocumentData.LocationName', render: rowData => <b>{rowData.DocumentData.LocationName}</b> },
                      { title: 'Date', field: 'DocumentDate' },              
                    ]}
                    data= {this.state.Data}
                    onRowClick={(event, rowData, togglePanel, selectedRow) => {
                      this.getItemData(rowData.DocumentIdentifier, rowData['tableData']['id'], rowData.DocumentDate); 
                      togglePanel(); 
                      this.setState({ selectedRow : selectedRow })} 
                      }
                      
                      actions={[
                        {
                          icon: 'pageview',
                          tooltip: 'Scan Barcode',
                          onClick: (event, rowData, selectedRow) => {
                            this.getItemData(rowData.DocumentIdentifier, rowData['tableData']['id'], rowData.DocumentDate); 
                            this.setState({ selectedRow : selectedRow })
                            this.scanBarCode(rowData)
                          }
                        }
                      ]}
                    options={{
                      pageSize: 10,
                      headerStyle: {
                        backgroundColor: '#01579b',
                              color: '#FFF'
                      }, 
                    }}
                    detailPanel={(rowData, selectedRow) => {
                    return (
                      this.state.Data[rowData['tableData']['id'] ]['items'] ? 
                      <div className={classes.itemTable}>
                        <MaterialTable
                            columns={[
                              { title: 'Item Code', field: 'Code', render: rowData => <b>{rowData.Code}</b>  },
                              { title: 'Item Description', field: 'ItemDescription', render: rowData => <b>{rowData.ItemDescription}</b>  },
                              { title: 'Quantity', field: 'Quantity', type: 'numeric' }, 
                              { title: 'Status', field: 'Status', render: rowData => rowData.PrintCount + '/' + rowData.Quantity}              
                            ]}
                            data={this.state.Data[rowData['tableData']['id'] ]['items'] ? this.state.Data[rowData['tableData']['id'] ]['items'] : [] }
                            actions={[
                              rowData => ({
                                icon: 'print',
                                tooltip: 'Print Barcode',
                                onClick: (event, rowData) => this.handleClickOpen(rowData.Code, rowData.ItemDescription, rowData.Quantity, rowData.PrintCount),
                                disabled: this.context.state.UserRole < userRoles.ROLE_MANAGER && rowData.PrintCount >= rowData.Quantity
                              }),
                              rowData => ({
                                icon: 'remove_red_eye',
                                tooltip: 'View Printed Barcodes',
                                onClick: (event, rowData) => this.setBarcodeViewDialog(rowData),
                                disabled: rowData.PrintCount < 1
                              })
                            ]}
                          options={{
                            actionsColumnIndex: -1,
                            pageSize : this.state.ItemData.length,
                            headerStyle: {
                              backgroundColor: '#8D8A8A',
                              color: '#FFF'
                            }
                          }}
                            title={"Item list for voucher :" + (rowData.DocumentIdentifier.VoucherNo)  }
                        />
                      </div> : <h5 align="center"> loading...</h5>
                    )
                  }}
                    title="Inventory Transfer In"
                  />
                }
            
          
        </GridItem>
        <Dialog
          open={this.state.printDialogOpen}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
          className={classes.dialogBox}
        >
            <DialogPrint 
              item={this.state.itemCode}
              documentDate= {this.state.documentDate} 
              desc={this.state.itemDescription}
              quantity={this.state.quantity} 
              count={this.state.printCount}
              documentIdentifier={this.state.documentIdentifier} 
              onClose={this.handleClose} 
              onPrintSuccess={this.handlePrintSuccess}>
            </DialogPrint>
        </Dialog>
        {this.state.successSnack &&
          <Snackbar
                    place="br"
                    color="success"
                    icon={AddAlert}
                    message="Operation successfull!"
                    open={this.state.successSnack}
                    closeNotification={() => this.setState({successSnack:false})}
                    close
                />
      }
      {this.state.scanDialog &&
         <Dialog
         open={this.state.scanDialog}
         onClose={this.handleClose}
         aria-labelledby="responsive-dialog-title"
         className={classes.dialogBox}
       >
         <ScanDialog
         documentIdentifier={this.state.documentIdentifier}
         onClose={this.handleClose} 
         onPrintSuccess={this.handlePrintSuccess}
         >
         </ScanDialog>
         </Dialog>
      }
      {this.state.viewBarcodeDialog &&
         <Dialog
         open={this.state.viewBarcodeDialog}
         onClose={this.handleClose}
         aria-labelledby="responsive-dialog-title"
         className={classes.dialogBox}
       >
         <ReleasedItemsDialog
           releasedData={this.state.storedData}
           title="Stored Barcodes"></ReleasedItemsDialog>
         </Dialog>
      }
      </GridContainer>
    );
  }
}

InventoryTransferIn.contextType = Context;

export default withStyles(styles)(InventoryTransferIn);
