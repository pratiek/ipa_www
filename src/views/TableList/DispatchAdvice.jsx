import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import MaterialTable from 'material-table'
import Dialog from '@material-ui/core/Dialog';
import DialogPrint from "views/Dialog/Dialog.jsx"
import Snackbar from "components/Snackbar/Snackbar.jsx";
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import AddAlert from "@material-ui/icons/AddAlert";
import DispatchAdviceDialog from "views/Dialog/DispatchAdviceDialog.jsx"
import ReleasedItemsDialog from "views/Dialog/ReleasedItemsDialog.jsx"
import CircularProgress from '@material-ui/core/CircularProgress';


const Services = require('services/RemoteServices.jsx');

const styles = {
  cardCategoryWhite: {
    "&,& a,& a:hover,& a:focus": {
      color: "rgba(255,255,255,.62)",
      margin: "0",
      fontSize: "14px",
      marginTop: "0",
      marginBottom: "0"
    },
    "& a,& a:hover,& a:focus": {
      color: "#FFFFFF"
    }
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none",
    "& small": {
      color: "#777",
      fontSize: "65%",
      fontWeight: "400",
      lineHeight: "1"
    }
  },
  dialogBox: {
    minWidth: '800px'
  },
  itemTable: {
    padding: "0px 5px 0px 5px"
  },
  appBar: {
    position: 'relative',
  }
};

class DispatchAdvice extends React.Component {
  constructor(props) {
    super(props)
    this.handleClose = this.handleClose.bind(this)
    this.handlePrintSuccess = this.handlePrintSuccess.bind(this)
    this.state = {
      printDialogOpen: false,
      itemCode: null,
      itemDescription: '',
      quantity: null,
      documentIdentifier: null,
      successSnack: false,
      selectedRow: null,
      documentDate: '',
      Data: [],
      ItemData: [],
      searchDialog: false,
      searchItems: [],
      scanDADialog: false,
      releaseDialog: false,
      loading:true,
      releasedData: null
    };
  }

  handleClickOpen = (itemCode, itemDesc, quantity) => {
    this.setState({searchDialog : true})
    Services.getItemLocations([ {Code : itemCode} ]).then((response) => {
      this.setState({searchItems: response[itemCode]})
    })
    this.setState({ printDialogOpen: true, itemCode: itemCode, itemDescription: itemDesc, quantity: quantity });
  };

  handleReleaseBarcodeDialog(rowData) {
    this.setState({releaseDialog : !(this.state.releaseDialog), releasedData: rowData.ReleasedItems})
  };

  handleClose() {
    this.setState({ printDialogOpen: false, searchDialog: false, scanDADialog:false, releaseDialog:false });
  };


  handlePrintSuccess() {
    this.setState({ printDialogOpen: false, successSnack: true, scanDADialog: false });
    this.fetchDispatchData();
    setTimeout(function(){
      this.setState({successSnack: false});
    }.bind(this),6000);
  };

  setDate (DocumentDate) {
    const month = DocumentDate.substr(0, DocumentDate.indexOf('/'))
    const year = DocumentDate.substr(DocumentDate.lastIndexOf("/") +1, DocumentDate.lastIndexOf('/')).slice(-3)
    const finalDate = month + year
    this.setState({ documentDate: finalDate });
  };
  
  scanBarCode(rowData) {
    this.setState({scanDADialog: true})
  };

  getItemData (documentIdentifier, rowIdx, documentDate) {
    this.setState({ documentIdentifier: documentIdentifier });
    if(this.state.Data[rowIdx]['items'] && this.state.Data[rowIdx]['items'].length > 0)
        return;
    
    Services.getDispatchAdviceItems(documentIdentifier).then(function(grnItems){
      this.setState({ ItemData: grnItems });
      var newData = this.state.Data;
      newData[rowIdx]['items'] = grnItems;
      this.setState({Data : newData});
    }.bind(this))
  };

  componentDidMount() {
   this.fetchDispatchData();
  }

  fetchDispatchData() {
    Services.getDispatchAdvice().then(function(grn){
      this.setState({ Data: grn, loading: false });
    }.bind(this))
  }


  render () {
    const { classes } = this.props;
    return (
      <GridContainer>
        <GridItem xs={12} sm={12} md={12}>
        { this.state.loading && 
          <div align="center">
            <CircularProgress
                  /><br />
                  Loading... Please wait
          </div>
                }
          { !(this.state.loading) && 
            <MaterialTable
            columns={[
              { title: 'Voucher Number', field: 'DocumentIdentifier.VoucherNo', render: rowData => <b>{rowData.DocumentIdentifier.VoucherNo}</b>  },
              { title: 'Customer Name', field: 'DocumentData.CustomerName', render: rowData => <b>{rowData.DocumentData.CustomerName}</b>  },
              { title: 'Date', field: 'DocumentDate', type: 'numeric' }
                        ]}
            data= {this.state.Data}
            onRowClick={(event, rowData, togglePanel, selectedRow) => {
               this.getItemData(rowData.DocumentIdentifier, rowData['tableData']['id'], rowData.DocumentDate); 
               togglePanel(); 
               this.setState({ selectedRow : selectedRow })} 
              }
              actions={[
                {
                  icon: 'pageview',
                  tooltip: 'Scan Barcode',
                  onClick: (event, rowData, selectedRow) => {
                    this.getItemData(rowData.DocumentIdentifier, rowData['tableData']['id'], rowData.DocumentDate); 
                    this.setState({ selectedRow : selectedRow })
                    this.scanBarCode(rowData)
                  }
                }
              ]}
            options={{
              pageSize: 10,
              headerStyle: {
                backgroundColor: '#01579b',
                color: '#FFF'
              }, 
            }}
            detailPanel={(rowData, selectedRow) => {
            return (
              this.state.Data[rowData['tableData']['id'] ]['items'] ? 
              <div className={classes.itemTable}>
                <MaterialTable
            columns={[
              { title: 'Item Code', field: 'Code',  render: rowData => <b>{rowData.Code}</b>  },
              { title: 'Item Description', field: 'ItemDescription',  render: rowData => <b>{rowData.ItemDescription}</b>  },
              { title: 'Quantity', field: 'Quantity', type: 'numeric' },
              { title: 'Status', field: 'PrintCount', type: 'numeric',  render: rowData => rowData.PrintCount + '/' + rowData.Quantity }              
            ]}
            data={this.state.Data[rowData['tableData']['id'] ]['items'] ? this.state.Data[rowData['tableData']['id'] ]['items'] : [] }
            actions={[
              {
                icon: 'list',
                tooltip: 'View Details',
                onClick: (event, rowData) => this.handleClickOpen(rowData.Code, rowData.ItemDescription, rowData.Quantity)
              },
              {
                icon: 'playlist_add_check',
                tooltip: 'View Details',
                onClick: (event, rowData) => this.handleReleaseBarcodeDialog(rowData)
              }
            ]}
          options={{
            actionsColumnIndex: -1,
            pageSize : this.state.ItemData.length,
            headerStyle: {
              backgroundColor: '#8D8A8A',
              color: '#FFF'
            }
          }}
            title={"Item list for voucher :" + (rowData.DocumentIdentifier.VoucherNo)  }
          />
              </div> : <h5 align="center"> loading...</h5>
            )
          }}
            title="Dispatch Advice"
        /> }
          
        </GridItem>
        <Dialog
          open={this.state.searchDialog}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
          fullScreen
        >
            <div>
            <AppBar className={classes.appBar}>
          <Toolbar>
            <IconButton edge="start" color="inherit" onClick={this.handleClose} aria-label="Close">
              <CloseIcon />
            </IconButton>
          </Toolbar>
        </AppBar>
            <MaterialTable
              columns={[
                { title: 'Item Code', field: 'ItemCode' },
                { title: 'Item Desc', field: 'ItemDesc' },
                { title: 'Item Barcode', field: 'ItemBarcode'},
                { title: 'Location Barcode', field: 'LocationBarcode'},
                { title: 'First Seen Date', field: 'FirstSeenDate'},
                { title: 'Last Activity Date', field: 'LastActivityDate'},
              ]}
              data={this.state.searchItems}
              title="View Items"
              options={{
                pageSize : 7
              }}
        />
            </div>
        </Dialog>
        {this.state.releaseDialog &&
         <Dialog
            open={this.state.releaseDialog}
            onClose={this.handleClose}
            aria-labelledby="responsive-dialog-title"
            className={classes.dialogBox}
          >
           <ReleasedItemsDialog
           releasedData={this.state.releasedData}
           title="Released Barcodes"></ReleasedItemsDialog>
         </Dialog>
      }
        {this.state.scanDADialog &&
         <Dialog
            open={this.state.scanDADialog}
            onClose={this.handleClose}
            aria-labelledby="responsive-dialog-title"
            className={classes.dialogBox}
          >
            <DispatchAdviceDialog
              documentIdentifier={this.state.documentIdentifier}
              onClose={this.handleClose} 
              onPrintSuccess={this.handlePrintSuccess}
            >
            </DispatchAdviceDialog>
         </Dialog>
      }
        {this.state.successSnack &&
          <Snackbar
                    place="br"
                    color="success"
                    icon={AddAlert}
                    message="Operation Successfull!"
                    open={this.state.successSnack}
                    closeNotification={() => this.setState({successSnack:false})}
                    close
                />
      }
      </GridContainer>
    );
  }
}

export default withStyles(styles)(DispatchAdvice);
