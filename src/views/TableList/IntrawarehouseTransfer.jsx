import React from 'react';
import Button from "components/CustomButtons/Button.jsx";
import ScanDialog from "views/Dialog/ScanDialog.jsx"
import Dialog from '@material-ui/core/Dialog';
import Snackbar from "components/Snackbar/Snackbar.jsx";
import AddAlert from "@material-ui/icons/AddAlert";



class IntrawarehouseTransfer extends React.Component{
    constructor(props) {
        super(props);
    this.handlePrintSuccess = this.handlePrintSuccess.bind(this)

        this.state = {
            scanDialog: false,
            documentIdentifier: {
                DocumentType: 'INTRA_WAREHOUSE_TRANSFER'
            },
            successSnack: false
        }
    }

    setScanDialog() {
        this.setState({scanDialog: true})
    }

    handleClose() {
        this.setState({scanDialog: false})
    }

    handlePrintSuccess() {
        this.setState({ printDialogOpen: false, successSnack: true, scanDialog: false });
    setTimeout(function(){
      this.setState({successSnack: false});
    }.bind(this),6000);
      };

    render() {
        const { classes } = this.props;
        return (
            <div align="center">
                 <Button color="primary" align="center" onClick={this.setScanDialog.bind(this)}>
                     Create New
                </Button>
            {this.state.scanDialog &&
                <Dialog
                open={this.state.scanDialog}
                onClose={this.handleClose.bind(this)}
                aria-labelledby="responsive-dialog-title"
            >
                <ScanDialog
                documentIdentifier={this.state.documentIdentifier}
                onClose={this.handleClose.bind(this)} 
                onPrintSuccess={this.handlePrintSuccess}
                >
                </ScanDialog>
                </Dialog>
            }
            {this.state.successSnack &&
          <Snackbar
                    place="br"
                    color="success"
                    icon={AddAlert}
                    message="Operation successfull!"
                    open={this.state.successSnack}
                    closeNotification={() => this.setState({successSnack:false})}
                    close
                />
            }

            </div>
        );
    }
}

export default IntrawarehouseTransfer;