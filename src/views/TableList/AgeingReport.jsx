import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles"
// @material-ui/icons
import Chip from '@material-ui/core/Chip';
import ReportEngine from "views/Reports/ReportEngine.jsx";
import Button from "components/CustomButtons/Button.jsx";
// core components
import { MTableToolbar } from 'material-table';
import Select from 'react-select';
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import Dialog from '@material-ui/core/Dialog';
import MaterialTable from "material-table";
import { Icon } from "@material-ui/core";

const Services = require('services/RemoteServices.jsx');
const style = {
    selectField: {
        marginTop: '10px',
        marginBottom: '10px'
      },
      selectDropdown: {
          'z-index': '100',
          position: 'relative'
      }
}

class Report extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            reportData: [],
            ERPLocations: [],
            selectedUserLocId: null,
            viewBarCodesList: false,
            itemsList: [],
            DropdownItems:[],
            selectedItem:null,
            IsShown:false
        }
    }

    componentWillMount() {
        Services.getStoredERPLocations().then((response)=> {
            var newResponse = this.getResponseWithAddedValue(response)
            this.setState({ERPLocations: newResponse})
           })
    }
    
    getResponseWithAddedValue(response) {
        var newResponse = response;
        for(var i=0; i<newResponse.length ; i++) {
           newResponse[i].value = i
        }
        return newResponse
      }

     handleUserLocSelectChange = (selectedUserLocId) => {
        this.setState({ selectedUserLocId });
        this.setState({selectedItem:null});
        //this.getStatusReport(selectedUserLocId.ID)
        this.getItemsFromErpLocation(selectedUserLocId.ID)
    }
    handleItemSelectChange = (selectedItem)=>{
        this.setState({ selectedItem });
    }

    getItemsFromErpLocation(id)
    {
        Services.getItemsFromErpLocation(id).then((response) => {
            this.setState({DropdownItems: response})
        })
    }

    getAgeingReport=() =>{
        var itemCode=this.state.selectedItem===null?"":this.state.selectedItem.ItemCode;
        var id=this.state.selectedUserLocId===null?0:this.state.selectedUserLocId.ID;
        Services.getAgeingReport(id,itemCode).then((response) => {
            this.setState({reportData: response})
        })
    }

    handleClose() {
        this.setState({viewBarCodesList: false})
    }

    setViewDialog(locId,storageDate) {
        var itemCode=this.state.selectedItem===null?"":this.state.selectedItem.ItemCode;
        this.getItemsBarcodes(locId,itemCode,storageDate);
        this.setState({viewBarCodesList: true});
    }

    getItemsBarcodes(locId,itemCode,storageDate) {

        Services.getStoredBarcodesListForAgeing(locId,itemCode,storageDate).then((response) => {
            this.setState({itemsList: response})
        })
    }

    render () {
        const {classes} = this.props;
        const reportdata = this.state.reportData;
        const statusColor = {
            Empty: 'Secondary',
            Occupied: 'primary'
        }
        const statusIcon = {
            Empty: 'close',
            Occupied: 'done'
        }
        return (
            <div>
                {
                    reportdata &&
                        <div>
                            <ReportEngine
                            title="Ageing Report Report"
                            columns={[
                                { title: 'Location Id', field: 'LocationId' },
                                { title: 'Status', field: 'Status', render: rowData =>
                                <Chip
                                    size="small"
                                    icon={<Icon small>{statusIcon[rowData.Status]}</Icon>}
                                    label={rowData.Status}                            
                                    color={statusColor[rowData.Status]}
                                />},
                                { title: 'Item Count', field: 'ItemCount', render: rowData =>
                                <>
                                <Chip
                                    size="small"
                                    label={rowData.ItemCount}                 
                                />
                                <IconButton color="primary" onClick={() => {this.setViewDialog(rowData.LocationId,rowData.storage_date)}} disabled={!rowData.ItemCount} className={classes.button} aria-label="add an alarm">
                                <Icon>remove_red_eye</Icon>
                              </IconButton> 
                              </>
                              },   
                                { title: 'Barcode', field: 'Barcode' },
                                { title: 'Date', field: 'storage_date',  render: rowData => rowData.storage_date ? rowData.storage_date : 'Not Available!' }
                            ]}
                            data = {reportdata}
                            components={{
                                Toolbar: props => (
                                <div className={classes.selectDropdown}>
                                    <MTableToolbar {...props} />
                                    <div style={{padding: '0px 10px', 'z-index': 0, position:'relative'}}>
                                    <Divider></Divider>
                                    <GridContainer>
                                        <GridItem xs={6} sm={6} md={4}>                   
                                        <Select
                                        placeholder ="Please Select Location"
                                            options={this.state.ERPLocations}
                                            getOptionLabel={(option)=>option.Name}
                                            value={this.state.selectedUserLocId}
                                            onChange={this.handleUserLocSelectChange}
                                            className={classes.selectField}
                                        />
                                    </GridItem>
                                    <GridItem xs={6} sm={6} md={4}>  
                                    <Select
                                        placeholder ="Please Select Item"
                                            options={this.state.DropdownItems}
                                            getOptionLabel={(option)=> option.ItemDesc}
                                            value={this.state.selectedItem}
                                            onChange={this.handleItemSelectChange}
                                            className={classes.selectField}
                                        />
                                     </GridItem>
                                    
                                    <Button style={{padding:'5px 24px', margin:'10px'}} color="primary"  align="right" 
                                    onClick={this.getAgeingReport}
                                    >
                                     Filter
                                    </Button>
                                    </GridContainer>
                                    </div>
                                </div>
                                ),
                            }}   
                            >
                                
                            </ReportEngine>
                        </div>
                    }                  
                <Dialog
                    open={this.state.viewBarCodesList}
                    onClose={this.handleClose.bind(this)}
                    aria-labelledby="responsive-dialog-title"
                    className={classes.dialogBox}
                >
                    <MaterialTable
                    columns={[
                        { title: "Barcode", field: "Barcode" },
                        { title: "Item Description", field: "ItemDesc" },
                    ]}
                    data={this.state.itemsList}
                    title="Item Barcode List"
                    options={{
                        pageSize: 5,
                        headerStyle: {
                            backgroundColor: '#01579b',
                            color: '#FFF'
                        }, 
                        }}
                    />
                </Dialog>
            </div>
        );
    }
}

export default withStyles(style)(Report);










