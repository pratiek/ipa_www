import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles"
import MaterialTable, { MTableToolbar } from 'material-table';

const style = {
}

const Services = require('services/RemoteServices.jsx');

class ReportEngine extends React.Component {
    constructor(props) {
        super(props);
    }


    render () {
      const {classes} = this.props;
        return (
            <MaterialTable
              title={this.props.title}
              columns={this.props.columns}
              data={this.props.data}     
              options={{
                pageSize: 10,
                exportButton: true,
                exportAllData: true,
                headerStyle: {
                  backgroundColor: '#01579b',
                        color: '#FFF'
                }
              }}
              components={this.props.components}
            />
          );
     }
}

export default withStyles(style)(ReportEngine);