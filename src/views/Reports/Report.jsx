import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles"
// @material-ui/icons
import LocationCity from "@material-ui/icons/LocationCity";
import TimeLapse from "@material-ui/icons/Timelapse";


// core components
import Tabs from "components/CustomTabs/CustomTabs.jsx";
import Dialog from '@material-ui/core/Dialog';
import MaterialTable from "material-table";

import LocationStatusReport from "views/TableList/LocationStatusReport.jsx";
import AgeingReport from "views/TableList/AgeingReport.jsx";



const style = {
    selectField: {
        marginTop: '10px',
        marginBottom: '10px'
      },
      selectDropdown: {
          'z-index': '100',
          position: 'relative'
      }
}



class Report extends React.Component {
    constructor(props) {
        super(props)
    }


    render () {
        const {classes} = this.props;
        return (
            <div>
                    <Tabs
                        headerColor="info"
                        tabs={[
                            {
                            tabName: "Location Status Report",
                            tabIcon: LocationCity,
                            tabContent: (
                                <LocationStatusReport></LocationStatusReport>
                            )
                            },
                            {
                            tabName: "Ageing Report",
                            tabIcon: TimeLapse,
                            tabContent: (
                                <div>
                                    <AgeingReport></AgeingReport>
                                </div>
                            )
                            }
                        ]}
                    />
            </div>
        );
    }
}

export default withStyles(style)(Report);