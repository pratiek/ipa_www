import React from "react";
import MaterialTable from 'material-table'

const Services = require('services/RemoteServices.jsx');

class ReleasedItemsDialog extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            barcodeData: this.props.releasedData
        }
    }

    render() {
        return (
            <div>
                <MaterialTable 
                    columns={[
                        { title: 'Barcode', field: 'Barcode' }
                    ]}
                    data={this.state.barcodeData}
                    title={this.props.title}
                    options={{
                        headerStyle: {
                            backgroundColor: '#01579b',
                                  color: '#FFF'
                          }
                      }}
                    />
            </div>
        );
    }
}

export default ReleasedItemsDialog;