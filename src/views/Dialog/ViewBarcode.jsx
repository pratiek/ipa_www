import React from "react";
import MaterialTable from 'material-table'
import CircularProgress from '@material-ui/core/CircularProgress';
import { withStyles } from "@material-ui/core";

const Services = require('services/RemoteServices.jsx');
const styles = {
    loadingOptions: {
        margin: '25px'
    }
}
class ViewBarcode extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            barcodeData: [],
            loading: true
        }
    }

    componentDidMount() {
        var itemCode = this.props.itemCode;
        Services.getBarcodeList(itemCode).then((response) => {
            this.setState({barcodeData: response, loading: false})
        })
    }

    render() {
        const { classes } = this.props;
        return (
            <div>
                {
                    this.state.loading && 
                    <div align="center" className={classes.loadingOptions}>
                    <CircularProgress
                          /><br />
                          Loading... Please wait
                    </div>
                }

                {
                    !(this.state.loading) &&
                    <MaterialTable 
                        columns={[
                            { title: 'Serial Number', field: 'serialNumber' },
                            { title: 'Barcode', field: 'barcode' }
                        ]}
                        data={this.state.barcodeData}
                        title="View Barcodes"
                        options={{
                            headerStyle: {
                                backgroundColor: '#01579b',
                                    color: '#FFF'
                            }
                        }}
                    />
                }
                
            </div>
        );
    }
}

export default withStyles(styles)(ViewBarcode);