import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import Button from "components/CustomButtons/Button.jsx";
import "assets/css/material-dashboard-react.css?v=1.6.0";
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import PrintIcon from '@material-ui/icons/Print';
import TextField from '@material-ui/core/TextField';
import FormControl from '@material-ui/core/FormControl';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Divider from '@material-ui/core/Divider'
import Select from 'react-select';
import Context from "Context.js";
import Snackbar from "components/Snackbar/Snackbar.jsx";
import AddAlert from "@material-ui/icons/AddAlert";
const userRoles = require('../../consts/userRoles')
const utils = require('../../utils/utils')
const Services = require('services/RemoteServices.jsx');

const styles = {
  formFields: {
    width: '100%',
    margin: '0px',
    padding: '0px',
    marginBottom: '12px'
  },
  formControl: {
    minWidth: '100%',
    'margin-top': '5px',
    'margin-bottom': '10px'
  },
  title: {
    width: '350px'
  },
  dialogActions: {
    background: 'lightgrey',
    margin: '0px',
    'z-index': '-1',
    padding: '5px'
  },
  dialogContent: {
    height: '365px'
  }
};

class Dialog extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      optionalEdit: false,
      subComponentCheck: false,
      snackBar: false,
      quantity: this.props.quantity,
      subComponentData: '',
      printMode: null,
      printModes: [],
      isLoading: false,
      selectedOption: null,
      codeRed: false,
      codeRedMessage: ''
    }
  }
  

  handleChangeIndex = index => {
    this.setState({ optionalEdit: !(this.optionalEdit) });
  };

  handleChange = name => event => {
    this.setState({ [name]: event.target.checked });
  };

  handleSelectChange = (selectedOption) => {
    this.setState({ selectedOption });
  }

  handleChangePrintMode = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleDialogClose = () => {
    this.props.onClose()
  };

  handleSuccess = () => {
    this.props.onPrintSuccess()
  };

  handleQuantityChange = quantity => event => {
    this.setState({ quantity: event.target.value });
  };

  handleSubComponentDataChange = subComponentData => event => {
    this.setState({ subComponentData: event.target.value });
  };

  handleSubmit = event => {
    event.preventDefault();
    this.setState({ isLoading: true })
    const data = {
      DocumentIdentifier: this.props.documentIdentifier,
      printDetails: {
        itemCode : this.props.item,
        quantity : this.state.quantity == null ? parseInt(this.props.quantity) :parseInt(this.state.quantity),
        itemDesc: this.props.desc,
        date: this.props.documentDate,
        printLayout: this.state.selectedOption,
        subComponentData: {
          hasSubComponent: this.state.subComponentCheck,
          data: this.state.subComponentData
        }
      } 
    };
    Services.printBarCode(data).then(function(responseBarcode){
      Services.getBarcodeHTML(responseBarcode.InstanceId).then((response) => {
        console.log(responseBarcode)
        this.setState({ isLoading: false });
        this.handleSuccess()
        var url = window.app.env.API_URL + 'Barcode/GetBarcodeHTML?instanceId='+responseBarcode.InstanceId
        window.open(url, '_blank');
      })
    }.bind(this))
    .catch(error => {
      this.setState({codeRed: true, codeRedMessage: error})
    });
  }

  componentDidMount() {
    Services.getPrintModes().then(function(response){
      var newResponse = this.getResponseWithAddedValue(response.Layouts)
      this.setState({printModes: newResponse})
    }.bind(this))
    this.printButtonCheck()
  }

  
  getResponseWithAddedValue(response) {
    var newResponse = response;
    for(var i=0; i<newResponse.length ; i++) {
       newResponse[i].value = i
    }
    return newResponse
  }

  printButtonCheck() {
    var totalQuantityCount = parseInt(this.props.count) + parseInt(this.state.quantity);
    if (totalQuantityCount > this.props.quantity && this.context.state.UserRole < userRoles.ROLE_MANAGER || (this.state.selectedOption == null)) {
      return true
    } else {
      return false
    }  
  };

  render () {
    const { classes } = this.props;
    const editStatus = this.state.subComponentCheck
    const { isLoading } = this.state;
    return (
      <div>
        {this.state.codeRed && 
                <Snackbar
                place="bl"
                color="danger"
                icon={AddAlert}
                message={this.state.codeRedMessage}
                open={this.state.codeRed}
                closeNotification={() => this.setState({ codeRed: false })}
                close
              />
                }
        <DialogTitle id="responsive-dialog-title" className={classes.title}>
          {/* {utils.checkAccess(context.getUser(), userRoles.ROLE_ADMIN)}{

          } */}
          {"Print Barcode"}
        </DialogTitle>
        <DialogContent className={classes.dialogContent}>
          <DialogContentText>
          <FormControl className={classes.formControl}>
            <TextField
              id="standard-read-only-input"
              label="Item Code"
              defaultValue={this.props.item}
              className={classes.formFields}
              margin="normal"
              InputProps={{
                disabled: true
              }}
            /> 
          </FormControl>
          <br />
         <TextField
          id="standard-read-only-input"
          label="Item Description"
          defaultValue={this.props.desc}
          className={classes.formFields}
          margin="normal"
          InputProps={{
            disabled: true
          }}
          />
          <br />
          <TextField
          id="standard-read-only-input"
          label="Month/Year of Entry"
          defaultValue={this.props.documentDate}
          className={classes.formFields}
          margin="normal"
          InputProps={{
            disabled: true
          }}
          />
          <br />
          <TextField
            id="standard-read-only-input"
            label="Copies"
            onChange={this.handleQuantityChange('quantity')}
            defaultValue={this.props.quantity}
            className={classes.formFields}
            margin="normal"
            InputProps={{
              readOnly: false
            }}
          /> 
          <br />
          <FormControl className={classes.formControl}>
              <Select
              placeholder="Select Print Mode"
                options={this.state.printModes}
                getOptionLabel={(option)=>option.Name}
                value={this.state.selectedOption}
                onChange={this.handleSelectChange}
              />
            </FormControl>
             <br/>
            <FormControlLabel
                control={
                  <Checkbox
                    checked={this.state.subComponentCheck}
                    onChange={this.handleChange('subComponentCheck')}
                    value="subComponentCheck"
                    color="primary"
                      />
                    }
                  label="Has Sub Components?"
            /> 
            <br/>
            {editStatus &&
                <TextField
                id="standard-read-only-input"
                label="Sub Component"
                defaultValue={this.props.subComponentData}
                onChange={this.handleSubComponentDataChange('subComponentData')}
                className={classes.formFields}
                margin="normal"
              />
            }
        </DialogContentText>
      </DialogContent>
      <Divider></Divider>
      <DialogActions className={classes.dialogActions}>
        <div>
          <Button 
            color="primary" 
            variant="primary"
            disabled={this.printButtonCheck()}
            onClick={!isLoading ? this.handleSubmit : null}
          >
          <PrintIcon></PrintIcon>
          {isLoading ? 'Printing....' : 'Print'}
          </Button>
          <Button onClick={this.props.onClose} color="grey">
            Close
          </Button>
        </div>
      </DialogActions>
      </div>
);
  }
}

Dialog.contextType = Context;

export default withStyles(styles)(Dialog);
