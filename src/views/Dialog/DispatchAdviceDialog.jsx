import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import Button from "components/CustomButtons/Button.jsx";
import "assets/css/material-dashboard-react.css?v=1.6.0";
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import SendIcon from '@material-ui/icons/Send';
import TextField from '@material-ui/core/TextField';
import MaterialTable, { MTableToolbar } from 'material-table';
import { array } from "prop-types";
import DispatchAdvice from "../TableList/DispatchAdvice";
import Snackbar from "components/Snackbar/Snackbar.jsx";
import AddAlert from "@material-ui/icons/AddAlert";
import Select from 'react-select';

const Services = require('services/RemoteServices.jsx');

const styles = {
  formFields: {
    width: '100%',
    margin: '0px',
    padding: '0px',
    marginBottom: '12px',
    marginTop: '15px'
  },
  title: {
    width: '450px'
  },
  dialogActions: {
    background: 'lightgrey',
    margin: '0px',
    'z-index': '-1',
    padding: '5px'
  }
};

class DispatchAdviceDialog extends React.Component {
  constructor(props) {
    super(props)
    this.handleSuccess = this.handleSuccess.bind(this)
    this.handleBarCodeSubmit = this.handleBarCodeSubmit.bind(this)
    this.state = {
      dialog: true,
      scannedBarCodes: '',
      scannedFormatedBarCodes: [],
      showResponseTable: false,
      responseData: {},
      responseTableData: [],
      checkValidBoolean : false,
      subComponentCheck: false,
      snackBar: false,
      quantity: null,
      subComponentData: '',
      printMode: null,
      isLoading: false,
      codeRed: false,
      codeRedMessage: '',
      ERPLocations: [],
      selectedOption: null
    }
  }

  componentDidMount() {
    this.getERPLocation()
  }

  handleSelectChange = (selectedOption) => {
    this.setState({ selectedOption });
  }

  handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

  handleSuccess = () => {
    this.props.onPrintSuccess()
  };

  getERPLocation () {
    Services.getStoredERPLocations().then((response)=> {
     var newResponse = this.getResponseWithAddedValue(response)
     this.setState({ERPLocations: newResponse})
    })
  }

  getResponseWithAddedValue(response) {
    var newResponse = response;
    for(var i=0; i<newResponse.length ; i++) {
       newResponse[i].value = i
    }
    return newResponse
  }

  deleteRow(rowData) {
    var actionData = this.state.responseTableData
    actionData.splice(rowData.tableData.id, 1)
    this.setState({ responseTableData: actionData })
    this.checkValid()
  };

  handleSubmit = event => {
    this.state.scannedBarCodes.split('\n').map((item, i) => {
      if(item) this.state.scannedFormatedBarCodes.push(item)
    });
    Services.submitScannedBarCodes(this.state.scannedFormatedBarCodes).then(function(response){
      this.setState({ isLoading: false, showResponseTable: true, responseData: response });

      var SortedBarcodePairs =[];
      var rowCount = -1;
      var rowInfo = {};
      this.state.scannedFormatedBarCodes.forEach((item) => {
        var barcodeInfo = response[item];
        if(barcodeInfo) {
          if(barcodeInfo.Type == 'Item'){
            SortedBarcodePairs.push(barcodeInfo);
          }
        }
      })
      var itemBarCode = ''
      var locationBarCode = ''
      var finalTableData = []
      var itemDescription = ''
      SortedBarcodePairs.forEach((item) => {
        itemDescription = item.PrintInstance.ItemDesc ? item.PrintInstance.ItemDesc : 'Not Available!' 
        if(item) {
          itemBarCode = item.Barcode
        } else {
          itemBarCode = ''
        }
        if ( !(itemBarCode === "")) {
          finalTableData.push({ ItemBarcode: itemBarCode, itemDescription: itemDescription })
        }      
      })
      this.setState({responseTableData: finalTableData})
      this.checkValid()
    }.bind(this))
    .catch(error => {
      this.setState({codeRed: true, codeRedMessage: error})
    });
    setTimeout(function(){
      this.setState({codeRed: false});
    }.bind(this),6000);
  }
  handleBarCodeSubmit() {
    let data = {
      itemsMoved: this.state.responseTableData,
      RefDocIdentifier: JSON.stringify(this.props.documentIdentifier),
      RestrictedWarehouseId: this.state.selectedOption.ID
    }
    Services.storeDispatchAdviceItems(data).then(function(response){
      this.handleSuccess()
    }.bind(this))
    .catch(error => {
      this.setState({codeRed: true, codeRedMessage: error});
    });
    setTimeout(function(){
      this.setState({codeRed: false});
    }.bind(this),6000);
  };
  checkValid() {
    var responseLength = this.state.responseTableData.length
    for(var item of this.state.responseTableData) {
      if(item.ItemBarcode === "" || item.LocationBarcode === "" || !(this.state.responseTableData[0]))  {
        this.setState({checkValidBoolean: true})
        break;
      }
      else {
        this.setState({checkValidBoolean: false})
      }
    }
  };
  

  render () {
    const { classes } = this.props;
    const editStatus = this.state.subComponentCheck
    const { isLoading } = this.state;
    return (
      <div>
        {this.state.codeRed && 
                <Snackbar
                place="bl"
                color="danger"
                icon={AddAlert}
                message={this.state.codeRedMessage}
                open={this.state.codeRed}
                closeNotification={() => this.setState({ codeRed: false })}
                close
              />
                }
        {
          !(this.state.showResponseTable) && 
          <div>
              <DialogTitle id="responsive-dialog-title" className={classes.title}>
                {"Scan Barcodes"}
               </DialogTitle>
                <DialogContent>
                  <DialogContentText>
                  <Select
                          placeholder ="Please Select Location"
                            options={this.state.ERPLocations}
                            getOptionLabel={(option)=>option.Name}
                            value={this.state.selectedOption}
                            onChange={this.handleSelectChange}
                            className={classes.selectField}
                          />
                  <TextField
                            id="standard-read-only-input"
                            label="Click before scanning"
                            onChange={this.handleChange('scannedBarCodes')}
                            className={classes.formFields}
                            margin="normal"
                            InputProps={{
                              readOnly: false,
                              multiline: true,
                              rows: 4
                            }}
                            />
                            
                </DialogContentText>
              </DialogContent>
              <DialogActions className={classes.dialogActions}>
                { this.state.scannedBarCodes && this.state.selectedOption &&
                <div>
                  <Button 
                  color="primary" 
                  variant="primary"
                  disabled={isLoading}
                  onClick={!isLoading ? this.handleSubmit : null}
                  >
                  <SendIcon></SendIcon>
                  {isLoading ? 'Sending....' : 'Submit'}
                  </Button>
                </div>
                }
                <Button onClick={this.props.onClose} color="grey">
                    Close
                  </Button>
              </DialogActions>
          </div>
          }
          {
            this.state.showResponseTable && 
            <div> 
              <MaterialTable
                  title="Validation"
                  columns={[
                    { title: 'Item Barcode', field: 'ItemBarcode' },
                    { title: 'Item Description', field: 'itemDescription' }
                  ]}
                  data={this.state.responseTableData}
                  options={{
                    actionsColumnIndex: -1,
                    headerStyle: {
                      backgroundColor: '#01579b',
                      color: '#FFF'
                    }
                  }}
                  actions={[
                    {
                      icon: 'delete',
                      tooltip: 'Delete Row',
                      onClick: (event, rowData) => this.deleteRow(rowData)
                    }
                  ]}
                />
                <div style={{textAlign:"right", margin: "5px"}} className={classes.DialogActions}>
                <Button 
                  onClick={this.handleBarCodeSubmit}
                  disabled={this.state.checkValidBoolean}
                  color="primary"
                  >
                    Save
                  </Button>
                <Button onClick={this.props.onClose} color="grey">
                    Close
                  </Button>
                  </div>
            </div>
          }
      </div>
);
  }
}

export default withStyles(styles)(DispatchAdviceDialog);
