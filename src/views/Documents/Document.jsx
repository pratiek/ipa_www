import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles"
// core components
import Tabs from "components/CustomTabs/CustomTabs.jsx";
import TableList from "views/TableList/TableList.jsx";
import SalesReturn from "views/TableList/SalesReturn.jsx";
import DispatchAdvice from "views/TableList/DispatchAdvice.jsx";
import InventoryTransferIn from "views/TableList/InventoryTransferIn.jsx";
import InventoryTransferOut from "views/TableList/InventoryTransferOut.jsx";
import PurchaseReturn from "views/TableList/PurchaseReturn.jsx";
import IntrawarehouseTransfer from "views/TableList/IntrawarehouseTransfer.jsx";


const style = {
}

class Document extends React.Component {
    constructor(props) {
        super(props)
    }
    render () {
        return (
            <Tabs
                headerColor="info"
                tabs={[
                    {
                    tabName: "Goods Reciept",
                    tabContent: (
                        <TableList></TableList>
                    )
                    },
                    {
                    tabName: "Sales Return",
                    tabContent: (
                        <SalesReturn></SalesReturn>
                    )
                    },
                    {
                    tabName: "Dispatch Advice",
                    tabContent: (
                        <DispatchAdvice></DispatchAdvice>
                    )
                    },
                    {
                    tabName: "Purchase Return",
                    tabContent: (
                        <PurchaseReturn></PurchaseReturn>
                    )
                    },
                    {
                    tabName: "Inventory Transfer In",
                    tabContent: (
                        <InventoryTransferIn></InventoryTransferIn>
                    )
                    },
                    {
                    tabName: "Inventory Transfer Out",
                    tabContent: (
                        <InventoryTransferOut></InventoryTransferOut>
                    )
                     },
                     {
                     tabName: "Intra Warehouse Transfer",
                     tabContent: (
                        <IntrawarehouseTransfer></IntrawarehouseTransfer>
                     )
                      }
                ]}
            />
        );
    }
}

export default withStyles(style)(Document);