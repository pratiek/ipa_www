import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Button from "components/CustomButtons/Button.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import Snackbar from "components/Snackbar/Snackbar.jsx";
import AddAlert from "@material-ui/icons/AddAlert";
import MaterialTable, { MTableToolbar } from 'material-table';
import Select from 'react-select';
import Divider from '@material-ui/core/Divider';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import PrintIcon from '@material-ui/icons/Print';

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  },
  formFields: {
    width: '90%'
  },
  Divider: {
    marginBottom: '10px',
    marginLeft: '0px',
    marginTop: '20px'
  },
  formFields: {
    width: '90%',
    marginTop: '25px'
  },
  selectField: {
    marginTop: '10px',
    marginBottom: '10px'
  },
  selectDropdown: {
    'z-index': '100',
    position: 'relative'
  },
  addButton: {
    marginTop: '8px'
  },
  dialogBox: {
    minWidth: '500px',
    'z-index': '100'
  },
  dialogContent: {
    minWidth: '300px',
    height: '100px'
  }
};

const Services = require('services/RemoteServices.jsx');

class LocationManagement extends React.Component {
  constructor(props) {
    super(props)
    this.sendLocation = this.sendLocation.bind(this)
    this.setPrintDialog = this.setPrintDialog.bind(this)
    this.state = {
      locationDetails: {
        aisle: null,
        block: null,
        side: null,
        rackFrom: null,
        rackTo: null,
        layerFrom: null,
        layerTo: null,
        partitionFrom: null,
        partitionTo: '',
      },
      successSnack: '',
      responseMessage: '',
      responseColor: '',
      addMode: false,
      locationTableData: [],
      ERPLocations: [],
      selectedUserLocId: null,
      submitted: false,
      codeRed: false,
      codeRedMessage: '',
      printerSelectDialog: false,
      printerList: [],
      selectedOption: null,
      currentRowData: null,
      bulkPrint: false
      };
  }

  clearForm () {
    // this.refs.form.reset();
  };

  handleUserLocSelectChange = (selectedUserLocId) => {
    this.setState({ selectedUserLocId });
    this.getLocationList(selectedUserLocId.ID)
    //this.getStatusReport(selectedUserLocId.ID)
} 

  handleChange = name => event => {
    const { locationDetails } = this.state;
    locationDetails[name] = event.target.value;
    this.setState({ locationDetails });
  };
  componentDidMount() {
    this.getERPLocation()
    ValidatorForm.addValidationRule('validNumberLength2', (value) => {
      if (value.length <= 2) {
          return true;
      }
      return false;
    });
    ValidatorForm.addValidationRule('validNumberLength1', (value) => {
      if (value.length <= 1) {
          return true;
      }
      return false;
    });
   };
   

   printButtonCheck() {
     var stateValue = this.state.selectedOption
     if (stateValue) return false
     else return true
   }

   getResponseWithAddedValue(response) {
    var newResponse = response;
    for(var i=0; i<newResponse.length ; i++) {
       newResponse[i].value = i
    }
    return newResponse
  }
 
   getLocationList (id) {
     Services.getLocation(id).then((response) => {
       this.setState({locationTableData: response})
     })
   }

   toggleMode() {
    this.setState({addMode: false})
   };

   getERPLocation () {
     Services.getStoredERPLocations().then((response)=> {
      var newResponse = this.getResponseWithAddedValue(response)
      this.setState({ERPLocations: newResponse})
     })
   }

   setAddMode() {
    this.setState({ addMode: true })
   }

   handleClose() {
     this.setState({printerSelectDialog:false})
   }

   setPrintDialog(rowData) {
    Services.getPrintModes().then(function(response){
      var newResponse = this.getResponseWithAddedValue(response.Layouts)
      this.setState({printerList: newResponse, printerSelectDialog: true, currentRowData: rowData})
    }.bind(this))
   }
    
  getResponseWithAddedValue(response) {
    var newResponse = response;
    for(var i=0; i<newResponse.length ; i++) {
       newResponse[i].value = i
    }
    return newResponse
  }

  handleSelectChange = (selectedOption) => {
    this.setState({ selectedOption });
  }

  sendLocation () {
    this.setState({submitted:true})
    console.log(this.state.locationDetails)
      var locationData = {
          Aisle: this.state.selectedUserLocId.ID,
          Block: this.state.locationDetails.block,
          Side: this.state.locationDetails.side,
          Rack: {
              From: this.state.locationDetails.rackFrom ,
              To: this.state.locationDetails.rackTo
          },
          Layer: {
            From: this.state.locationDetails.layerFrom ,
            To: this.state.locationDetails.layerTo 
          },
          Partition: {
            From: this.state.locationDetails.partitionFrom,
            To: this.state.locationDetails.partitionTo
          }
      }
    Services.createLocation(locationData).then(function(response){
      if(response) this.setState({ successSnack: true, responseMessage: 'Location created successfully!', addMode: false, responseColor: 'success' }); this.getLocationList(this.state.selectedUserLocId.ID);
      if(!response) this.setState({ successSnack: true, responseMessage: 'There is some problem creating location!', responseColor: 'danger' });
    }.bind(this))
    .catch(error => {
      this.setState({codeRed: true, codeRedMessage: error})
    });
    setTimeout(function(){
      this.setState({successSnack: false, codeRed: false});
    }.bind(this),6000);
  };


  onRowClick(event, rowData, togglePanel){
    var newTableData = this.state.locationTableData;
    var locationId = this.state.selectedUserLocId.ID;
    Services.getAllLocations(locationId, rowData.Block, rowData.Side).then((response) => {
      for(var x = 0; x<response.length; x++){
        response[x].parent = rowData
      }

      newTableData[rowData.tableData.id].items = response;
      this.setState({locationTableData: newTableData})
    })
    togglePanel();
  }

  printSingleLocationBarcode(){
    var locdata = this.state.currentRowData
    console.log(locdata)
   var params =  {
            "Aisle":this.state.selectedUserLocId.ID,
            "Block":locdata.parent.Block,
            "Side":locdata.parent.Side,
            "Rack":{"From":locdata.Rack,"To":locdata.Rack},
            "Layer":{"From":locdata.Layer,"To":locdata.Layer},
            "Partition":{"From":locdata.Partition,"To":locdata.Partition},
            "printMode": this.state.selectedOption.LayoutId
          }

    Services.printLocations(params).then((res)=> {
      var url = window.app.env.API_URL + 'Storage/PrintLocations?locParams='+JSON.stringify(params)
      window.open(url, '_blank');
      this.setState({responseMessage: 'Barcode Created Successfully!', successSnack: true, responseColor: 'success', printerSelectDialog: false})
      setTimeout(function(){
        this.setState({successSnack: false});
      }.bind(this),6000);
    });
  }

  printAllLocationBarcodes(){
    var locdata = this.state.currentRowData
    var params =  {
      "Aisle":this.state.selectedUserLocId.ID,
      "Block":locdata.Block,
      "Side":locdata.Side,
      "Rack":{"From":locdata.MinRack,"To":locdata.MaxRack},
      "Layer":{"From":locdata.MinLayer,"To":locdata.MaxLayer},
      "Partition":{"From":locdata.MinPartition,"To":locdata.MaxPartition},
      "printMode": this.state.selectedOption.LayoutId
    }
    Services.printLocations(params).then((res)=> {
      var url = window.app.env.API_URL + 'Storage/PrintLocations?locParams='+JSON.stringify(params)
      window.open(url, '_blank');
      this.setState({responseMessage: 'Barcode Created Successfully!', successSnack: true,  responseColor: 'success', printerSelectDialog: false})
      setTimeout(function(){
        this.setState({successSnack: false});
      }.bind(this),6000);
    });
  }

  render () {
    const { classes } = this.props;
    const {locationDetails} = this.state;
    return (
      <div>
         {this.state.codeRed && 
                <Snackbar
                place="bl"
                color="danger"
                icon={AddAlert}
                message={this.state.codeRedMessage}
                open={this.state.codeRed}
                closeNotification={() => this.setState({ codeRed: false })}
                close
              />
                }
          {/* <GridItem xs={12} sm={12} md={12}> */}
          { this.state.addMode &&
          <div>
            <ValidatorForm
                ref="form"
                onSubmit={this.sendLocation}
                onError={errors => console.log(errors)}
                  >
              <CardBody>  
              <Button className="float-right" color="primary" onClick={this.toggleMode.bind(this)}>Go Back</Button>
              <Divider variant="inset"  className={classes.Divider}/>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                          Select Location 
                          <Select
                            options={this.state.ERPLocations}
                            getOptionLabel={(option)=>option.Name}
                            value={this.state.selectedERPLocation}
                            onChange={this.handleUserLocSelectChange}
                            className={classes.selectField}
                          />
                      </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                    <TextValidator
                    label="Building"
                    onChange={this.handleChange('block')}
                    name="block"
                    className={classes.formFields}
                    value={locationDetails.block}
                    validators={['required', 'validNumberLength2']}
                    errorMessages={['Building is required', 'Maximum of two characters is allowed']}
                />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                  <TextValidator
                    label="Floor"
                    onChange={this.handleChange('side')}
                    name="side"
                    className={classes.formFields}
                    value={locationDetails.side}
                    validators={['required', 'validNumberLength2']}
                    errorMessages={['Floor is required', 'Maximum of two characters is allowed']}
                />
                  </GridItem>
                  
                  </GridContainer>
                  <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                      <h3>
                      Select Row
                      </h3>
                      </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                  <TextValidator
                    label="Row From"
                    onChange={this.handleChange('rackFrom')}
                    name="rackFrom"
                    className={classes.formFields}
                    value={locationDetails.rackFrom}
                    validators={['required', 'isNumber']}
                    errorMessages={['Row From is required', 'Please enter valid number']}
                />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                  <TextValidator
                    label="Row To"
                    onChange={this.handleChange('rackTo')}
                    name="rackTo"
                    className={classes.formFields}
                    value={locationDetails.rackTo}
                    validators={['required', 'isNumber']}
                    errorMessages={['Row To is required', 'Please enter valid number']}
                />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                      <h3>
                      Select Rack
                      </h3>
                      </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                  <TextValidator
                    label="Rack From"
                    onChange={this.handleChange('layerFrom')}
                    name="layerFrom"
                    className={classes.formFields}
                    value={locationDetails.layerFrom}
                    validators={['required', 'isNumber']}
                    errorMessages={['Rack From is required', 'Please enter valid number']}
                />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                  <TextValidator
                    label="Rack To"
                    onChange={this.handleChange('layerTo')}
                    name="layerTo"
                    className={classes.formFields}
                    value={locationDetails.layerTo}
                    validators={['required', 'isNumber']}
                    errorMessages={['Rack To is required', 'Please enter valid number']}
                />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={4}>
                      <h3>
                      Select Layer
                      </h3>
                      </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                  <TextValidator
                    label="Layer From"
                    onChange={this.handleChange('partitionFrom')}
                    name="partitionFrom"
                    className={classes.formFields}
                    value={locationDetails.partitionFrom}
                    validators={['required', 'validNumberLength1']}
                    errorMessages={['Layer From is required', 'Only single character is allowed.']}
                />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={4}>
                  <TextValidator
                    label="Layer To"
                    onChange={this.handleChange('partitionTo')}
                    name="partitionTo"
                    className={classes.formFields}
                    value={locationDetails.partitionTo}
                    validators={['required', 'validNumberLength1']}
                    errorMessages={['Layer To is required', 'Only single character is allowed.']}
                />
                  </GridItem>
                </GridContainer>
              </CardBody>
              <Divider variant="inset"  className={classes.Divider}/>
              <CardFooter>
              <Button type="submit" color="primary">Create Location</Button>
              </CardFooter>
              </ValidatorForm>
              </div>
          }
          {
            !(this.state.addMode) && 
            <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
            <MaterialTable
              columns={[
                { title: 'Block', field: 'Block' },
                { title: 'Floor', field: 'Side' },
                { title: 'Rows', field: 'Rack', render: rowData => rowData.MinRack + ' - ' + rowData.MaxRack },
                { title: 'Rack', field: 'Layer',  render: rowData => rowData.MinLayer + ' - ' + rowData.MaxLayer },
                { title: 'Layer', field: 'Partition', render: rowData => rowData.MinPartition + ' - ' + rowData.MaxPartition },
              ]}
              data={this.state.locationTableData}
              title="Location Management"
              options={{
                pageSize: 10,
                actionsColumnIndex: -1,
                headerStyle: {
                  backgroundColor: '#01579b',
                        color: '#FFF'
                }, 
              }}
              detailPanel={rowData => {
                return (
                  <MaterialTable
                  columns={[
                    { title: 'Row', field: 'Rack' },
                    { title: 'Rack', field: 'Layer' },
                    { title: 'Layer', field: 'Partition' },
                    { title: 'Barcode', field: 'Barcode' },
                  ]}
                  data={this.state.locationTableData[rowData.tableData.id].items}
                  title="Details"
                  actions={[
                    {
                          icon: 'print',
                          tooltip: 'Print Barcode',
                          onClick: (event, rowData) => {
                            this.setPrintDialog(rowData)
                            this.setState({bulkPrint: false})
                          }
                        }
                  ]}
                  options={{
                    pageSize: 10,
                    actionsColumnIndex: -1,
                    headerStyle: {
                      backgroundColor: '#8D8A8A',
                            color: '#FFF'
                    }, 
                  }}
                />
                )
              }}
              onRowClick={this.onRowClick.bind(this)}
              actions={[
                {
                      icon: 'print',
                      tooltip: 'Print',
                      onClick: (event, rowData) => {
                        this.setPrintDialog(rowData)
                        this.setState({bulkPrint: true})
                      }
                    }
              ]}
              components={{
                Toolbar: props => (
                  <div className={classes.selectDropdown}>
                    <MTableToolbar {...props} />
                    <Divider></Divider>
                    <div style={{padding: '0px 10px', 'z-index': 0, position:'relative'}}>
                      <GridContainer>
                    <GridItem xs={12} sm={12} md={4}>  
                          <Select
                          placeholder ="Please Select Location"
                            options={this.state.ERPLocations}
                            getOptionLabel={(option)=>option.Name}
                            value={this.state.selectedUserLocId}
                            onChange={this.handleUserLocSelectChange}
                            className={classes.selectField}
                          />
                          </GridItem>
                          <GridItem xs={12} sm={12} md={8} align="right">  
                            <Button size="small" type="submit" onClick={this.setAddMode.bind(this)} className={classes.addButton} color="primary">Create New</Button>
                          </GridItem>
                      </GridContainer>
                    </div>
                  </div>
                ),
              }}   
            />
            </GridItem>
        </GridContainer>
          }
        {this.state.successSnack &&
          <Snackbar
                    place="br"
                    color={this.state.responseColor}
                    icon={AddAlert}
                    message={this.state.responseMessage}
                    open={this.state.successSnack}
                    closeNotification={() => this.setState({successSnack:false})}
                    close
                />
      }
      {
        this.state.printerSelectDialog   &&
        <Dialog
         open={this.state.printerSelectDialog}
         onClose={this.handleClose.bind(this)}
         aria-labelledby="responsive-dialog-title"
         className={classes.dialogBox}
       >
          <DialogTitle id="responsive-dialog-title" className={classes.title}>Select Printer</DialogTitle>
          <DialogContent className={classes.dialogContent}>
          <DialogContentText>
         <Select
                placeholder="Select Print Mode"
                options={this.state.printerList}
                getOptionLabel={(option)=>option.Name}
                value={this.state.selectedOption}
                onChange={this.handleSelectChange}
                className={classes.selectDropdown}
              />          
            </DialogContentText>
          </DialogContent>
          <Divider></Divider>
          <DialogActions className={classes.dialogActions}>
        <div>
          <Button 
            color="primary" 
            variant="primary"
            disabled={this.printButtonCheck()}
            onClick={this.state.bulkPrint ? this.printAllLocationBarcodes.bind(this) : this.printSingleLocationBarcode.bind(this)}
          >
          <PrintIcon></PrintIcon>
          Print
          </Button>
          <Button onClick={this.handleClose.bind(this)} color="grey">
            Close
          </Button>
        </div>
      </DialogActions>
         </Dialog>
      }
      </div>
    );
  }
}

export default withStyles(styles)(LocationManagement);
