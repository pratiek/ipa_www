import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import InputLabel from "@material-ui/core/InputLabel";
// core components
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import CustomInput from "components/CustomInput/CustomInput.jsx";
import Button from "components/CustomButtons/Button.jsx";
import Card from "components/Card/Card.jsx";
import CardHeader from "components/Card/CardHeader.jsx";
import CardAvatar from "components/Card/CardAvatar.jsx";
import CardBody from "components/Card/CardBody.jsx";
import CardFooter from "components/Card/CardFooter.jsx";
import TextField from '@material-ui/core/TextField';
import Snackbar from "components/Snackbar/Snackbar.jsx";
import AddAlert from "@material-ui/icons/AddAlert";
import MaterialTable, { MTableToolbar } from 'material-table';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { ValidatorForm, TextValidator} from 'react-material-ui-form-validator';
import FormControl from '@material-ui/core/FormControl';
import Select from 'react-select';
import Chip from '@material-ui/core/Chip';
import FaceIcon from '@material-ui/icons/Face';
import Badge from '@material-ui/core/Badge';
import avatar from "assets/img/faces/marc.jpg";
import { Divider } from "@material-ui/core";

const utils = require('../../utils/utils')

const styles = {
  cardCategoryWhite: {
    color: "rgba(255,255,255,.62)",
    margin: "0",
    fontSize: "14px",
    marginTop: "0",
    marginBottom: "0"
  },
  cardTitleWhite: {
    color: "#FFFFFF",
    marginTop: "0px",
    minHeight: "auto",
    fontWeight: "300",
    fontFamily: "'Roboto', 'Helvetica', 'Arial', sans-serif",
    marginBottom: "3px",
    textDecoration: "none"
  },
  formFields: {
    width: '90%',
    marginTop: '25px'
  },
  buttonsRight : {
    align: 'right'
  },
  formControl: {
    width: '90%',
    'margin-top': '26px',
    'margin-bottom': '12px'
  },
  chip: {
    marginRight: '20px'
  },
  dialog: {
    minWidth: '500px'
  },
  dialogActions: {
    background: 'lightgrey',
    margin: '0px',
    'z-index': '-1',
    padding: '5px'
  }
};

const Services = require('services/RemoteServices.jsx');

class CreateUsers extends React.Component {
  constructor(props) {
    super(props)
    this.sendUser = this.sendUser.bind(this)
    this.changeListStatus = this.changeListStatus.bind(this)
    this.handleClose = this.handleClose.bind(this)
    this.deleteUser = this.deleteUser.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.state = {
      userDetails: {
        userName: '',
        password: '',
        email: '',
        firstName: '',
        lastName: '',
        contactNumber: null
      },
      listUsers: true,
      successSnack: '',
      signUpMessage: '',
      signUpMessageColor: '',
      usersList: [],
      confirmationDialog: false,
      currentUserRowData: null,
      companyList: [],
      branchList: [],
      selectedCompanyCode : '',
      selectedCompanyOption: null,
      fetchBranch: true,
      selectedBranchOption: null,
      selectedUserRole: null,
      userRoles : [
        { name: 'Basic', value: 1 },
        { name: 'Manager', value: 2 },
        { name: 'Admin', value: 3 }
      ],
      submitted: false,
      codeRed: false,
      codeRedMessage: '',
      changePasswordDialog: false,
      newPassword: '',
      currentUpdateUserId: null
      };
  }

  clearForm () {
    this.refs.form.reset();
  };

  handleChange = name => event => {
    const { userDetails } = this.state;
    userDetails[name] = event.target.value;
    this.setState({ userDetails });
  };

  handlePasswordChange = name => event => {
    this.setState({[name]: event.target.value})
  };


  changeListStatus() {
    this.setState({listUsers : !(this.state.listUsers)})
  };

  handleCompanySelectChange = (selectedCompanyOption) => {
    this.setState({ selectedCompanyOption });
  }

  handleUserRoleSelectChange = (selectedUserRole) => {
    this.setState({ selectedUserRole });
  }

  handleBranchSelectChange = (selectedBranchOption) => {
    this.setState({ selectedBranchOption });
  }

  updateUserPassword(userId) {
    var data = {
      UserId: this.state.currentUpdateUserId,
      Password: this.state.newPassword
    }
    Services.updateUserPassword(data).then((response) => {
      this.setState({changePasswordDialog: false, successSnack: true, signUpMessage: 'Successfully Updated', signUpMessageColor: 'success', newPassword: ''})
    })
    setTimeout(function(){
      this.setState({successSnack: false});
    }.bind(this),6000);
  }


  sendUser () {
    this.setState({submitted: true})
    var newData = {
      ...this.state.userDetails,
      branchCode: this.state.selectedBranchOption.BranchCode,
      companyCode: this.state.selectedCompanyOption.CompanyCode,
      branchName: this.state.selectedBranchOption.BranchName,
      companyName: this.state.selectedCompanyOption.CompanyName,
      userRole: this.state.selectedUserRole.value
    }
    Services.createUser(newData).then(function(response){
      if(response.success) this.setState({ successSnack: true, listUsers: true, signUpMessage: response.message, signUpMessageColor: 'success' }); this.getUsersList()
      if(!response.success) this.setState({ successSnack: true, signUpMessage: response.message, signUpMessageColor: 'danger' });
    }.bind(this))
    .catch(error => {
      this.setState({codeRed: true, codeRedMessage: error})
  });
    setTimeout(function(){
      this.setState({successSnack: false, codeRed: false});
    }.bind(this),6000);
  };

  getResponseWithAddedValue(response) {
    var newResponse = response;
    for(var i=0; i<newResponse.length ; i++) {
       newResponse[i].value = i
    }
    return newResponse
  }

  componentDidMount() {
   this.getUsersList()
   Services.getCompanyList().then((response) => {
     var newResponse = this.getResponseWithAddedValue(response)
     this.setState({companyList: newResponse})
     this.setFieldLookUp()
   })
   
  };

   async setFieldLookUp() {
    var branchLookup = {}
    var companyLookup = {}
    var companyList = this.state.companyList
    if(companyList) {
      companyList.forEach((item) => {
        companyLookup[item.CompanyCode] = item.CompanyName
        Services.getBranchList(item.CompanyCode).then((response) => {
          for (var i=0; i< response.length; i++) {
            var branchCode = response[i].BranchCode
            var branchName = response[i].BranchName
            branchLookup[branchCode] = branchName
          }
          this.setState({ companyLookup: companyLookup, branchLookup: branchLookup })
        })
      })
    }
    
  }

  getBranchList() {
    var companyCode = this.state.selectedCompanyOption.CompanyCode
    Services.getBranchList(companyCode).then((response) => {
      var newResponse = this.getResponseWithAddedValue(response)
      this.setState({branchList: newResponse, fetchBranch:false})
    })
  }


  getUsersList () {
    Services.getUsers().then((response) => {
      this.setState({usersList: response})
    })
  }

  deleteUser(rowData) {
    Services.deleteUser(this.state.currentUserRowData).then((response) => {
      this.setState({successSnack: true, signUpMessage: 'Deleted Successfully', signUpMessageColor: 'success', confirmationDialog: false})
      this.getUsersList()
    })
  }
  
  handleClose () {
    this.setState({confirmationDialog : false, changePasswordDialog:false})
  };



  render () {
    const { classes } = this.props;
    const {userDetails} = this.state;
    const rolesAssign = {
      1: 'Basic',
      2: 'Manager',
      3: 'Admin'
    }
    const rolesColor = {
      1: 'default',
      2: 'primary',
      3: 'secondary'
    }
    if(this.state.selectedCompanyOption && this.state.fetchBranch) {
      this.getBranchList()
    }
    var companyLookup = this.state.companyLookup
    var branchLookup = this.state.branchLookup
    var updateButton = true
    if(this.state.newPassword) updateButton = false;
    return (
      <div className="text-right">
         {this.state.codeRed && 
                <Snackbar
                place="bl"
                color="danger"
                icon={AddAlert}
                message={this.state.codeRedMessage}
                open={this.state.codeRed}
                closeNotification={() => this.setState({ codeRed: false })}
                close
              />
                }
         { !(this.state.listUsers) && 
        <GridContainer>
          <ValidatorForm
                ref="form"
                onSubmit={this.sendUser}
                onError={errors => console.log(errors)}
                  >
          <GridItem xs={12} sm={12} md={12}>
              <CardBody>
              <div className="text-right">
                  <Button className="float-right" color="primary" onClick={this.changeListStatus}>Go Back</Button>
                </div>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={6}>
                  <TextValidator
                    label="Username"
                    onChange={this.handleChange('userName')}
                    name="email"
                    className={classes.formFields}
                    value={userDetails.userName}
                    validators={['required']}
                    errorMessages={['Username is required']}
                />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                  <TextValidator
                    label="Password"
                    type="password"
                    onChange={this.handleChange('password')}
                    name="Password"
                    className={classes.formFields}
                    value={userDetails.password}
                    validators={['required']}
                    errorMessages={['Password is required']}
                />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                  <TextValidator
                    label="Email"
                    onChange={this.handleChange('email')}
                    name="email"
                    className={classes.formFields}
                    value={userDetails.email}
                    validators={['required', 'isEmail']}
                    errorMessages={['Email is required','Email is not valid']}
                />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                  <TextValidator
                    label="Contact Number"
                    onChange={this.handleChange('contactNumber')}
                    name="contactNumber"
                    className={classes.formFields}
                    value={userDetails.contactNumber}
                    validators={['required', 'isNumber']}
                    errorMessages={['Contact Number is required','Contact Number is not valid']}
                />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={6}>
                  <TextValidator
                    label="First Name"
                    onChange={this.handleChange('firstName')}
                    name="firstName"
                    className={classes.formFields}
                    value={userDetails.firstName}
                    validators={['required']}
                    errorMessages={['First Name is required']}
                />
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                  <TextValidator
                    label="Last Name"
                    onChange={this.handleChange('lastName')}
                    name="lastName"
                    className={classes.formFields}
                    value={userDetails.lastName}
                    validators={['required']}
                    errorMessages={['Last Name is required']}
                />
                  </GridItem>
                </GridContainer>
                <GridContainer>
                  <GridItem xs={12} sm={12} md={6}>
                  <FormControl className={classes.formControl}>
                      <Select
                      placeholder="Select Company"
                        options={this.state.companyList}
                        getOptionLabel={(option)=>option.CompanyName}
                        value={this.state.selectedOption}
                        onChange={this.handleCompanySelectChange}
                      />
                    </FormControl>
                  </GridItem>
                  <GridItem xs={12} sm={12} md={6}>
                      <FormControl className={classes.formControl}>
                          <Select
                          placeholder="Select User Role"
                            options={this.state.userRoles}
                            getOptionLabel={(option)=>option.name}
                            value={this.state.selectedUserRole}
                            onChange={this.handleUserRoleSelectChange}
                          />
                        </FormControl>
                      </GridItem>
                  </GridContainer>
                  <GridContainer>
                  {
                    !(this.state.fetchBranch) &&
                    <GridItem xs={12} sm={12} md={6}>
                      <FormControl className={classes.formControl}>
                          <Select
                            placeholder="Select Branch"
                            options={this.state.branchList}
                            getOptionLabel={(option)=>option.BranchName}
                            value={this.state.selectedOption}
                            onChange={this.handleBranchSelectChange}
                          />
                        </FormControl>
                      </GridItem>
                  }
                </GridContainer>
              </CardBody>
              <CardFooter>
                <Button type="submit" color="primary">Create User</Button>
              </CardFooter>
          </GridItem>
          </ValidatorForm>
        </GridContainer>
         }
        {this.state.successSnack &&
          <Snackbar
                    place="br"
                    color={this.state.signUpMessageColor}
                    icon={AddAlert}
                    message={this.state.signUpMessage}
                    open={this.state.successSnack}
                    closeNotification={() => this.setState({successSnack:false})}
                    close
                />
        }
        {this.state.listUsers && this.state.usersList &&
        <GridContainer>
          <GridItem xs={12} sm={12} md={12}>
          <MaterialTable
              title="Users"
              columns={[
                { title: 'Username', field: 'UserName', editable: 'never', render: rowData => <b>{rowData.UserName}</b> },
                { 
                  title: 'User Role', field: 'UserRole', render: rowData => 
                    <Chip
                    label={rolesAssign[rowData.UserRole]}
                    color={rolesColor[rowData.UserRole]}
                    /> ,
                    lookup: { 1: 'Basic', 2: 'Manager', 3: 'Admin' },
                },
                { title: 'Contact Number', field: 'ContactNumber' },
                { title: 'Company', field: 'CompanyCode', lookup: companyLookup },
                { title: 'Branch', field: 'BranchCode', editable: 'onUpdate', lookup: branchLookup },
                { title: 'Email', field: 'Email' }
              ]}
              data={this.state.usersList}  
              editable={{
                onRowUpdate: (newData, oldData) =>
                      Services.updateUser(newData).then((response) => {
                        this.getUsersList()
                        this.setState({signUpMessage: 'User Updated Successfulsly', successSnack: true, signUpMessageColor: 'success'})
                        setTimeout(function(){
                          this.setState({successSnack: false});
                        }.bind(this),6000);
                      })
              }} 
              components={{
                Toolbar: props => (
                  <div>
                    <MTableToolbar {...props} />
                    <Divider></Divider>
                    <div style={{padding: '5px 10px 5px 0px', textAlign: 'right'}}>
                    <Badge className={classes.chip} badgeContent={this.state.usersList.length} color="primary">
                    <Chip
                      icon={<FaceIcon />}
                      label="Total Users"
                    />
                    </Badge>
                      <Button type="submit" onClick={this.changeListStatus} color="primary">Add New</Button>
                    </div>
                  </div>
                ),
              }}     
              actions={[
                {
                      icon: 'delete',
                      tooltip: 'Delete User',
                      onClick: (event, rowData) => {
                        this.setState({confirmationDialog : true, currentUserRowData: rowData})
                      }
                    },
                    {
                      icon: 'security',
                      tooltip: 'Update Password',
                      onClick: (event, rowData) => {
                        this.setState({changePasswordDialog: true, currentUpdateUserId: rowData.UserId})
                      }
                    }
              ]}
              options={{
                pageSize: 10,
                headerStyle: {
                  backgroundColor: '#01579b',
                        color: '#FFF'
                }, 
              }}
            />
          </GridItem>
        </GridContainer>
        }
        {
          this.state.confirmationDialog &&
          <Dialog
              open={this.state.confirmationDialog}
              onClose={this.handleClose}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
            >
              <DialogTitle id="alert-dialog-title">{"CONFIRMATION"}</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                  Do you want to delete the user?
                </DialogContentText>
              </DialogContent>
              <DialogActions>
                <Button onClick={this.handleClose} color="primary">
                  Disagree
                </Button>
                <Button onClick={this.deleteUser} color="primary" autoFocus>
                  Agree
                </Button>
              </DialogActions>
            </Dialog>
        }
        {
          this.state.changePasswordDialog && 
          <Dialog
              open={this.state.changePasswordDialog}
              onClose={this.handleClose}
              aria-labelledby="alert-dialog-title"
              aria-describedby="alert-dialog-description"
              className={classes.dialog}
            >
              <DialogTitle id="alert-dialog-title">{"Update Password"}</DialogTitle>
              <DialogContent>
                <DialogContentText id="alert-dialog-description">
                <TextField
                  id="standard-name"
                  label="New Password"
                  className={classes.formFields}
                  value={this.state.newPassword}
                  onChange={this.handlePasswordChange('newPassword')}
                  margin="normal"
                />
                </DialogContentText>
              </DialogContent>
            <DialogActions >
                <Button onClick={this.updateUserPassword.bind(this)} color="primary" autoFocus disabled={updateButton}>
                  Update
                </Button>
                <Button onClick={this.handleClose} color="secondary">
                  Close
                </Button>
              </DialogActions>
            </Dialog>
        }
      </div>
    );
  }
}

export default withStyles(styles)(CreateUsers);
