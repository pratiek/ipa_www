import React from "react";
import Select from 'react-select';
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Button from "components/CustomButtons/Button.jsx";
import Typography from '@material-ui/core/Typography';
import withStyles from "@material-ui/core/styles/withStyles";
import CardBody from "components/Card/CardBody.jsx";
import Divider from '@material-ui/core/Divider';
import Icon from '@material-ui/core/Icon';
import TextField from '@material-ui/core/TextField';
import Snackbar from "components/Snackbar/Snackbar.jsx";
import AddAlert from "@material-ui/icons/AddAlert";
import Badge from '@material-ui/core/Badge';
import SnackbarContent from "components/Snackbar/SnackbarContent.jsx";
const Services = require('services/RemoteServices.jsx');

const styles = {
    card: {
        minWidth: 275,
        height: 235,
        overflowY: 'scroll'
      },
      bullet: {
        display: 'inline-block',
        margin: '0 2px',
        transform: 'scale(0.8)',
      },
      title: {
        fontSize: 14,
        marginRight: '10px'
      },
      pos: {
        marginBottom: 12,
      },
      addButton: {
          marginTop: 20
      },
      selectField: {
          marginTop: '35px'
      },
      formFields: {
          width: '100%'
      },
      chip: {
        marginLeft: '20px'
      },
      locationList: {
          padding: '7px'
      }
}

class LocationConfigure extends React.Component {
    constructor(props){
        super(props);
        this.addLocation = this.addLocation.bind(this)
        this.setCodeRed = this.setCodeRed.bind(this)
        this.state= {
            ERPLocations: [],
            selectedERPLocation: [],
            codeName: '',
            addedLocations: [],
            snackBar: false,
            codeRed: false,
            codeRedMessage: ''
        }
    }

    handleUserLocationSelectChange = (selectedERPLocation) => {
        this.setState({ selectedERPLocation });
      }

    componentWillMount() {
        this.fetchStoredLocations();
    }

    fetchStoredLocations() {
        Services.getStoredERPLocations().then((response) => {
            this.setState({addedLocations: response})
        })
    }

   componentDidMount() {
    Services.getERPLocations().then((response)=> {
        var newResponse = this.getResponseWithAddedValue(response)
        this.setState({ERPLocations: newResponse})
       })
   }

   getResponseWithAddedValue(response) {
    var newResponse = response;
    for(var i=0; i<newResponse.length ; i++) {
       newResponse[i].value = i
    }
    return newResponse
  }

   reset() {
    this.setState({selectedERPLocation: [], codeName: ''})
   };
   
   addLocation() {
       var selected = this.state.selectedERPLocation;
       var code = this.state.codeName;
       var data = {
            Identifier: selected.Identifier,
            Name: selected.Name,
            Code: code
       }
       Services.setStoredERPLocations(data).then((response) => {
           this.setState({snackBar: true});
           this.fetchStoredLocations();
           this.reset();
           setTimeout(function(){
            this.setState({snackBar: false});
          }.bind(this),6000);
       })
       .catch(error => {
        this.setCodeRed(error)
        setTimeout(function(){
            this.setState({codeRed: false});
          }.bind(this),9000);
    });
   }

   setCodeRed(message) {
       this.setState({codeRed: true, codeRedMessage: message})
   }

   handleChange = name => event => {
    this.setState({ [name]: event.target.value });
  };

    render() {
        const { classes } = this.props;
        const locations = this.state.addedLocations;
        const bull = <span className={classes.bullet}>•</span>;
        const listItems = locations.map((location) =>
            <div className={classes.locationList}>
                <li><b> {location.ID} :</b>  {location.Name ? location.Name : 'Not Available'}</li>
                <Divider></Divider>
                </div>
            );
        var booleanValue = true
       if (this.state.selectedERPLocation && this.state.codeName) {
        booleanValue = false
       } 
        return (
            <CardBody>
                {this.state.codeRed && 
                <Snackbar
                place="bl"
                color="danger"
                icon={AddAlert}
                message={this.state.codeRedMessage}
                open={this.state.codeRed}
                closeNotification={() => this.setState({ codeRed: false })}
                close
              />
                }
                <GridContainer>
                <GridItem xs={12} sm={12} md={6}> 
                          <Select
                          placeholder="Select Location To Add"
                            options={this.state.ERPLocations}
                            getOptionLabel={(option)=>option.Name}
                            value={this.state.selectedERPLocation}
                            onChange={this.handleUserLocationSelectChange}
                            className={classes.selectField}
                          />
                          <TextField
                            id="standard-read-only-input"
                            label="Code"
                            onChange={this.handleChange('codeName')}
                            className={classes.formFields}
                            margin="normal"
                            InputProps={{
                            readOnly: false
                            }}
                            /> 
                    <br/>
              <Button disabled={booleanValue} className={classes.addButton} color="primary" onClick={this.addLocation.bind(this)}>
                  Add
              </Button>
                </GridItem>
                <GridItem xs={12} sm={12} md={6}>
                <Card>
                    <CardContent>
                    <Badge className={classes.chip} badgeContent={listItems.length} color="primary">
                    <Typography className={classes.title} color="textSecondary" gutterBottom>
                        Added Locations
                    </Typography>
                    </Badge>
                    <div className="scrollbar">
                        <div className="overflow">
                        <Typography variant="body2" component="p">
                            {
                                locations &&
                                <div>
                                    {
                                     listItems
                                    }
                                </div>
                            }
                        </Typography>

                        </div>
                    </div>
                        
                        
                    </CardContent>
                    </Card>
                </GridItem>
                </GridContainer>
                {
                this.state.snackBar &&
                <Snackbar
                            place="br"
                            color="success"
                            icon={AddAlert}
                            message="Location Added Successfully"
                            open={this.state.snackBar}
                            closeNotification={() => this.setState({snackBar:false})}
                            close
                        />
                }
            </CardBody>
        );
    }
}

export default  withStyles(styles)(LocationConfigure);