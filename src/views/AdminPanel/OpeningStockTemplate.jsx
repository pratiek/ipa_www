import React from 'react'
import MaterialTable, { MTableToolbar } from 'material-table';
import CircularProgress from '@material-ui/core/CircularProgress';
import DialogPrint from "views/Dialog/Dialog.jsx"
import Dialog from '@material-ui/core/Dialog';
import { withStyles } from '@material-ui/core';
import Snackbar from "components/Snackbar/Snackbar.jsx";
import AddAlert from "@material-ui/icons/AddAlert";
import Button from "components/CustomButtons/Button.jsx";
import Chip from '@material-ui/core/Chip';
import Fab from '@material-ui/core/Fab';
import PrintIcon from '@material-ui/icons/Print';
import { Divider } from "@material-ui/core";
import ScanDialog from "views/Dialog/ScanDialog.jsx"

import GridItem from "components/Grid/GridItem.jsx";

const Services = require('services/RemoteServices.jsx');

const styles = {
  dialogBox: {
    minWidth: '500px'
  },
  Chip: {
    marginTop: '10px',
    marginLeft: '10px',
    marginBottom: '10px'
  }
}

class OpeningStockTemplate extends React.Component {
    constructor(props){
        super(props);
        this.handleClose = this.handleClose.bind(this)
        this.state = {
          stockData: [],
          loading: true,
          printDialogOpen: false,
          rowData: [],
          documentDate: '',
          printCount: 0,
          successSnack: false,
          documentIdentifier : {
            DocumentType: 'OPENING_STOCK'
          },
          scanDialog: false
        }
    }

    handleClose() {
      this.setState({ printDialogOpen: false, scanDialog: false });
    };

    setDate (DocumentDate) {
      var month = DocumentDate.substr(0, DocumentDate.indexOf('/'))
      var year = DocumentDate.substr(DocumentDate.indexOf("/") +1, DocumentDate.lastIndexOf('/')).slice(0, -2)
      if(month.length == 1) {
        month = '0'+ month
      }
      if(year.length == 1){
        year = '0'+year
      }
      if(year.length === 3){
        year = year.substring(0, 2);
      }
      
      const finalDate = month + year
      this.setState({ documentDate: finalDate });
    };

    setNewStockData(data) {
      this.setState({stockData: data})
    }

    handlePrintSuccess() {
      this.setState({ printDialogOpen: false, successSnack: true, scanDialog:false });
      setTimeout(function(){
        this.setState({successSnack: false});
      }.bind(this),6000);
    };
  
    componentDidMount() {
      Services.getOpeningStockTemplate().then((response) => {
        this.setState({stockData: response, loading: false})
      })
    }

    printSubmit(rowData) {
      this.setState({printDialogOpen: true, rowData: rowData})
      this.setDate(rowData.date)
    }

    setScanDialog() {
      this.setState({scanDialog: true})
    }

    render() {
      const { classes } = this.props;
      var newStockData = this.state.stockData
        return (
            <div>
              {
                this.state.loading &&
                <div align="center">
                  <CircularProgress
                        /><br />
                        Loading... Please wait
                </div>
              }
              {
                !(this.state.loading) && 
                <MaterialTable
                    columns={[
                    { title: "Item Code", field: "itemCode" },
                    { title: "Item Description", field: "itemDesc", render: rowData => <b>{rowData.itemDesc}</b> },
                    {title: "Quantity", field: "quantity", render: rowData =>  <Chip
                          label={rowData.quantity}
                        />,
                    },
                    { title: "Date", field: "date"}
                    ]}
                    data={this.state.stockData}
                    options={{
                        pageSize: 10,
                        actionsColumnIndex: -1,
                        headerStyle: {
                            backgroundColor: '#01579b',
                            color: '#FFF'
                        }
                      }}
                      components={{
                        Toolbar: props => (
                          <div>
                            <MTableToolbar {...props} />
                            <Divider></Divider>
                            <div style={{padding: '5px 10px 5px 0px', textAlign: 'right'}}>
                             <Button size="small" type="submit" onClick={this.setScanDialog.bind(this)} color="primary">Scan</Button>
                            </div>
                          </div>
                        ),
                      }}     
                      detailPanel={rowData => {
                        return (
                          <div>
                            <GridItem>
                            <Chip
                              label={"Status : " + newStockData[rowData.tableData.id].statusCount + "/"  + rowData.quantity}
                              color="secondary"
                              className={classes.Chip}
                            />
                            <Chip
                              label={"Quantity : " + rowData.quantity}
                              color="secondary"
                              className={classes.Chip}
                            />
                             <Fab
                                variant="extended"
                                size="small"
                                color="Success"
                                aria-label="Add"
                                className={classes.Chip}
                                onClick={ (rowdata) => this.printSubmit(rowData)}
                              >
                                <PrintIcon className={classes.extendedIcon} />
                              </Fab>
                               </GridItem>
                          </div>
                        )
                      }}
                    onRowClick={(event, rowData, togglePanel) =>{
                      var status = 0;
                        var docIdentifier = JSON.stringify(this.state.documentIdentifier)
                        Services.getOpeningStockItemStatus({docIdentifier}, rowData.itemCode).then((response) => {
                          newStockData[rowData.tableData.id].statusCount = parseInt(response) 
                          this.setNewStockData(newStockData)
                        })
                        togglePanel()
                    } }
                    title="Opening Stock Template"
                />
              }
               {this.state.scanDialog &&
         <Dialog
         open={this.state.scanDialog}
         onClose={this.handleClose}
         aria-labelledby="responsive-dialog-title"
         className={classes.dialogBox}
       >
          <ScanDialog
          documentIdentifier={this.state.documentIdentifier}
          onClose={this.handleClose.bind(this)} 
          onPrintSuccess={this.handlePrintSuccess.bind(this)}
          >
          </ScanDialog>
         </Dialog>
      }
        <Dialog
          open={this.state.printDialogOpen}
          onClose={this.handleClose}
          aria-labelledby="responsive-dialog-title"
          className={classes.dialogBox}
        > 
            <DialogPrint 
              item={this.state.rowData.itemCode}
              documentDate= {this.state.documentDate} 
              desc={this.state.rowData.itemDesc}
              quantity={this.state.rowData.quantity} 
              count={this.state.printCount}
              documentIdentifier={this.state.documentIdentifier} 
              onClose={this.handleClose} 
              onPrintSuccess={this.handlePrintSuccess.bind(this)}>
            </DialogPrint>
        </Dialog>
        {this.state.successSnack &&
          <Snackbar
                    place="br"
                    color="success"
                    icon={AddAlert}
                    message="Operation successfull!"
                    open={this.state.successSnack}
                    closeNotification={() => this.setState({successSnack:false})}
                    close
                />
      }
          </div> 
        );
    }
}

export default withStyles(styles)(OpeningStockTemplate);
