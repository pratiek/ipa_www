import React from "react";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles"
// @material-ui/icons
import BugReport from "@material-ui/icons/BugReport";
import Print from "@material-ui/icons/Print";
import Cloud from "@material-ui/icons/Cloud";
import AddCircle from "@material-ui/icons/AddCircle";
import LocationCity from "@material-ui/icons/LocationCity";
import List from "@material-ui/icons/List";
// core components
import Tabs from "components/CustomTabs/CustomTabs.jsx";
import Tasks from "components/Tasks/Tasks.jsx";
import CreateUsers from "views/AdminPanel/CreateUsers.jsx";
import LocationManagement from "views/AdminPanel/LocationManagement.jsx";
import OpeningStockTemplate from "views/AdminPanel/OpeningStockTemplate.jsx";
import LocationConfigure from "views/AdminPanel/LocationConfigure.jsx";
import PrintSettings from "views/AdminPanel/PrintSettings.jsx";


import { bugs, website, server } from "variables/general.jsx";

const style = {
}

class AdminPanel extends React.Component {
    constructor(props) {
        super(props)
    }
    render () {
        return (
            <Tabs
                headerColor="info"
                tabs={[
                    {
                    tabName: "Create Users",
                    tabIcon: AddCircle,
                    tabContent: (
                        <CreateUsers></CreateUsers>
                    )
                    },
                    {
                    tabName: "Location Configure",
                    tabIcon: LocationCity,
                    tabContent: (
                        <LocationConfigure></LocationConfigure>
                    )
                    },
                    {
                    tabName: "Location Management",
                    tabIcon: LocationCity,
                    tabContent: (
                        <LocationManagement></LocationManagement>
                    )
                    },
                    {
                    tabName: "Opening Stock Template",
                    tabIcon: List,
                    tabContent: (
                        <OpeningStockTemplate></OpeningStockTemplate>
                    )
                    }
                ]}
            />
        );
    }
}

export default withStyles(style)(AdminPanel);