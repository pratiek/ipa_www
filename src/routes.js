// @material-ui/icons
import Dashboard from "@material-ui/icons/Dashboard";
import Person from "@material-ui/icons/Person";
import LibraryBooks from "@material-ui/icons/LibraryBooks";
import BubbleChart from "@material-ui/icons/BubbleChart";
import LocationOn from "@material-ui/icons/LocationOn";
import Notifications from "@material-ui/icons/Notifications";
import Unarchive from "@material-ui/icons/Unarchive";
import Language from "@material-ui/icons/Language";
// core components/views for Admin layout
import DashboardPage from "views/Dashboard/Dashboard.jsx";
import UserProfile from "views/UserProfile/UserProfile.jsx";
import TableList from "views/TableList/TableList.jsx";
import SalesReturn from "views/TableList/SalesReturn.jsx";
import DispatchAdvice from "views/TableList/DispatchAdvice.jsx";
import InventoryTransferIn from "views/TableList/InventoryTransferIn.jsx";
import InventoryTransferOut from "views/TableList/InventoryTransferOut.jsx";
import PurchaseReturn from "views/TableList/PurchaseReturn.jsx";
import AdminPanel from "views/AdminPanel/AdminPanel.jsx";
import Report from "views/Reports/Report.jsx"
import Document from "views/Documents/Document.jsx";

import LoginPage from "views/Login/Login.jsx"
import Storage from "views/Storage/Storage.jsx"
import Typography from "views/Typography/Typography.jsx";
import Icons from "views/Icons/Icons.jsx";
import Maps from "views/Maps/Maps.jsx";
import NotificationsPage from "views/Notifications/Notifications.jsx";
import UpgradeToPro from "views/UpgradeToPro/UpgradeToPro.jsx";
// core components/views for RTL layout
import RTLPage from "views/RTLPage/RTLPage.jsx";
import userRoles from 'consts/userRoles.js';

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    rtlName: "لوحة القيادة",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin",
    hasChildMenu: false,
    minRole : userRoles.ROLE_BASIC
  },
  {
    path: "/Document",
    open: true,
    name: "View Documents",
    rtlName: "قائمة الجدول",
    icon: "content_paste",
    component: Document,
    layout: "/admin",
    minRole : userRoles.ROLE_BASIC,
    hasChildMenu: true,
    children: [
    {
      path: "/table",
      name: "Goods Reciept",
      rtlName: "لوحة القيادة",
      icon: 'play_arrow',
      component: TableList,
      layout: "/admin"
    },
    {
      path: "/SalesReturn",
      name: "Sales Return",
      rtlName: "لوحة القيادة",
      icon: 'play_arrow',
      component: SalesReturn,
      layout: "/admin"
    },
    {
      path: "/DispatchAdvice",
      name: "Dispatch Advice",
      rtlName: "لوحة القيادة",
      icon: 'play_arrow',
      component: DispatchAdvice,
      layout: "/admin"
    },
    {
      path: "/PurchaseReturn",
      name: "Purchase Return",
      rtlName: "لوحة القيادة",
      icon: 'play_arrow',
      component: PurchaseReturn,
      layout: "/admin"
    },
    ,
    {
      path: "/InventoryTransferIn",
      name: "Inventory Transfer In",
      rtlName: "لوحة القيادة",
      icon: 'play_arrow',
      component: InventoryTransferIn,
      layout: "/admin"
    },
    ,
    {
      path: "/InventoryTransferOut",
      name: "Inventory Transfer Out",
      rtlName: "لوحة القيادة",
      icon: 'play_arrow',
      component: InventoryTransferOut,
      layout: "/admin"
    }
  ]
  },
  // {
  //   path: "/table",
  //   name: "Goods Reciept",
  //   rtlName: "لوحة القيادة",
  //   icon: 'play_arrow',
  //   component: TableList,
  //   layout: "/admin"
  // },
  // {
  //   path: "/SalesReturn",
  //   name: "Sales Return",
  //   rtlName: "لوحة القيادة",
  //   icon: 'play_arrow',
  //   component: SalesReturn,
  //   layout: "/admin"
  // },
  // {
  //   path: "/DispatchAdvice",
  //   name: "Dispatch Advice",
  //   rtlName: "لوحة القيادة",
  //   icon: 'play_arrow',
  //   component: DispatchAdvice,
  //   layout: "/admin"
  // },
  // {
  //   path: "/PurchaseReturn",
  //   name: "Purchase Return",
  //   rtlName: "لوحة القيادة",
  //   icon: 'play_arrow',
  //   component: PurchaseReturn,
  //   layout: "/admin"
  // },
  // ,
  // {
  //   path: "/InventoryTransferIn",
  //   name: "Inventory Transfer In",
  //   rtlName: "لوحة القيادة",
  //   icon: 'play_arrow',
  //   component: InventoryTransferIn,
  //   layout: "/admin"
  // },
  // ,
  // {
  //   path: "/InventoryTransferOut",
  //   name: "Inventory Transfer Out",
  //   rtlName: "لوحة القيادة",
  //   icon: 'play_arrow',
  //   component: InventoryTransferOut,
  //   layout: "/admin"
  // },
  // {
  //   path: "/storage",
  //   name: "Storage",
  //   rtlName: "قائمة الجدول",
  //   icon: "store",
  //   component: Storage,
  //   layout: "/admin",
  //   hasChildMenu: false
  // }
  ,
  {
    path: "/Report",
    name: "Reports",
    rtlName: "قائمة الجدول",
    icon: "report",
    component: Report,
    layout: "/admin",
    hasChildMenu: false,
    minRole : userRoles.ROLE_BASIC
  }
  ,
  {
    path: "/AdminPanel",
    name: "Admin Panel",
    rtlName: "قائمة الجدول",
    icon: "store",
    component: AdminPanel,
    layout: "/admin",
    hasChildMenu: false,
    minRole : userRoles.ROLE_ADMIN
  },
  {
    path: "/login",
    name: "Logout",
    rtlName: "قائمة الجدول",
    icon: "person",
    component: LoginPage,
    layout: "/login",
    hasChildMenu: false
  }
  // {
  //   path: "/typography",
  //   name: "Typography",
  //   rtlName: "طباعة",
  //   icon: LibraryBooks,
  //   component: Typography,
  //   layout: "/admin"
  // },
  // {
  //   path: "/icons",
  //   name: "Icons",
  //   rtlName: "الرموز",
  //   icon: BubbleChart,
  //   component: Icons,
  //   layout: "/admin"
  // },
  // {
  //   path: "/maps",
  //   name: "Maps",
  //   rtlName: "خرائط",
  //   icon: LocationOn,
  //   component: Maps,
  //   layout: "/admin"
  // },
  // {
  //   path: "/notifications",
  //   name: "Notifications",
  //   rtlName: "إخطارات",
  //   icon: Notifications,
  //   component: NotificationsPage,
  //   layout: "/admin"
  // },
  // {
  //   path: "/upgrade-to-pro",
  //   name: "Upgrade To PRO",
  //   rtlName: "التطور للاحترافية",
  //   icon: Unarchive,
  //   component: UpgradeToPro,
  //   layout: "/admin"
  // },
  // {
  //   path: "/rtl-page",
  //   name: "RTL Support",
  //   rtlName: "پشتیبانی از راست به چپ",
  //   icon: Language,
  //   component: RTLPage,
  //   layout: "/rtl"
  // }
];

export default dashboardRoutes;
