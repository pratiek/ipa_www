import React from "react";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";
import Context from "Context.js";

// core components
import Admin from "layouts/Admin.jsx";
import RTL from "layouts/RTL.jsx";
import Login from "layouts/Login.jsx";

import "assets/css/material-dashboard-react.css?v=1.6.0";
import Index from 'index.js'

const hist = createBrowserHistory();

const token = localStorage.getItem('token');

class index extends React.Component {
  constructor(props){
    super(props);
    this.state = {
    }
  }

  render () {
    return (
      <div>
    <Context.Provider value={{
      state: this.state
    }}>
     <Router history={hist}>
    <div>
    {
    !token &&
    <Switch>
      <Redirect from="/" to="/login/user" />
    </Switch>
  }
    <Switch>
        <Route path="/admin" component={Admin} />
        <Route path="/rtl" component={RTL} />
        <Route path="/login" component={Login} />
        <Redirect from="/" to="/login/user" />
    </Switch>
  </div>
  </Router>
  </Context.Provider>
  </div>
    );
  }
}

export default index;

ReactDOM.render(<Index />,document.getElementById("root"));
