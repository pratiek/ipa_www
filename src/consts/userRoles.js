module.exports= Object.freeze({
    ROLE_BASIC : 1,
    ROLE_MANAGER : 2,
    ROLE_ADMIN : 3
})