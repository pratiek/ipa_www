import React from "react";
import classNames from "classnames";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import MenuItem from "@material-ui/core/MenuItem";
import MenuList from "@material-ui/core/MenuList";
import Grow from "@material-ui/core/Grow";
import Paper from "@material-ui/core/Paper";
import ClickAwayListener from "@material-ui/core/ClickAwayListener";
import Poppers from "@material-ui/core/Popper";
// @material-ui/icons

import headerLinksStyle from "assets/jss/material-dashboard-react/components/headerLinksStyle.jsx";
import Icon from '@material-ui/core/Icon';
import Fab from '@material-ui/core/Fab';
import { Redirect } from 'react-router';



class HeaderLinks extends React.Component {
  state = {
    open: false,
    username: localStorage.getItem('userDetails'),
    toLoginScreen: false
  };
  handleToggle = () => {
    this.setState(state => ({ open: !state.open }));
  };

  handleClose = event => {
    if (this.anchorEl.contains(event.target)) {
      return;
    }
    this.setState({ open: false });
  };

  getUser() {
    var username = localStorage.getItem('userDetails')
    this.setState({ username: username })
  };

  componentDidMount() {
   this.getUser()
  };

  // componentDidUpdate () {
  //   this.getUser()
  // };

  logout() {
    localStorage.removeItem('userDetails')
    localStorage.removeItem('firstLogin')
    localStorage.removeItem('token')
    this.setState({ toLoginScreen: true })
  };

  render() {
    if (this.state.toLoginScreen === true) {
      // component={Link} to="/admin/dashboard" 
      return <Redirect to='/login/login' />
    }
    const { classes } = this.props;
    const { open } = this.state;
    return (
      <div>
        <div className={classes.manager}>
        Welcome, {this.state.username} ! &nbsp;&nbsp;
          <Fab
          size="small"
            buttonRef={node => {
              this.anchorEl = node;
            }}
            color="secondary" aria-label="Add" onClick={this.handleClick}
            //color={window.innerWidth > 959 ? "transparent" : "white"}
            //justIcon={window.innerWidth > 959}
            //simple={!(window.innerWidth > 959)}
            //aria-owns={open ? "menu-list-grow" : null}
            //aria-haspopup="true"
            onClick={this.handleToggle}
            //className={classes.buttonLink}
          >
            {/* <Notifications className={classes.icons} /> */}
            {/* <span className={classes.notifications}>5</span> */}
            {/* <Hidden mdUp implementation="css">
              <p onClick={this.handleClick} className={classes.linkText}>
                Notification
              </p>
            </Hidden> */}
            {/* <Fab size="small" color="secondary" aria-label="Add" onClick={this.handleClick}>
              <Icon>settings</Icon>
            </Fab> */}
            <Icon>settings</Icon>
          </Fab>
          <Poppers
            open={open}
            anchorEl={this.anchorEl}
            transition
            disablePortal
            className={
              classNames({ [classes.popperClose]: !open }) +
              " " +
              classes.pooperNav
            }
          >
            {({ TransitionProps, placement }) => (
              <Grow
                {...TransitionProps}
                id="menu-list-grow"
                style={{
                  transformOrigin:
                    placement === "bottom" ? "center top" : "center bottom"
                }}
              >
                <Paper>
                  <ClickAwayListener onClickAway={this.handleClose}>
                    <MenuList role="menu">
                      <MenuItem
                        onClick={this.logout.bind(this)}
                        className={classes.dropdownItem}
                      >
                        Logout
                      </MenuItem>
                      {/* <MenuItem
                        onClick={this.handleClose}
                        className={classes.dropdownItem}
                      >
                        You have 5 new tasks
                      </MenuItem>
                      <MenuItem
                        onClick={this.handleClose}
                        className={classes.dropdownItem}
                      >
                        You're now friend with Andrew
                      </MenuItem>
                      <MenuItem
                        onClick={this.handleClose}
                        className={classes.dropdownItem}
                      >
                        Another Notification
                      </MenuItem>
                      <MenuItem
                        onClick={this.handleClose}
                        className={classes.dropdownItem}
                      >
                        Another One
                      </MenuItem> */}
                    </MenuList>
                  </ClickAwayListener>
                </Paper>
              </Grow>
            )}
          </Poppers>
        </div>
        {/* <Button
          color={window.innerWidth > 959 ? "transparent" : "white"}
          justIcon={window.innerWidth > 959}
          simple={!(window.innerWidth > 959)}
          aria-label="power_settings_new"
          className={classes.buttonLink}
        >
          <Person className={classes.icons} />
          <Hidden mdUp implementation="css">
            <p className={classes.linkText}>Profile</p>
          </Hidden>
        </Button> */}
        
        {/* <Fab size="small" color="secondary" aria-label="Add" onClick={this.logout.bind(this)}>
          <Icon>power_settings_new</Icon>
        </Fab> */}
      </div>
    );
  }
}

export default withStyles(headerLinksStyle)(HeaderLinks);
