import React from "react";
import Muted from "components/Typography/Muted.jsx";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = {
    mutedText: {
        color: 'red',
        whiteSpace : 'nowrap' ,
        width: '10px',
        overflow: 'hidden',
        textOverflow:'ellipsis',
        border: '1px solid #000000'
      }
};

class MutedDescEllipsis extends React.Component {
    render () {
        const { classes } = this.props;
        return (
            <Muted className={classes.mutedText}>{this.props.text}</Muted>
        );
    }
}

export default withStyles(styles)(MutedDescEllipsis);
