import React from "react";
import classNames from "classnames";
import PropTypes from "prop-types";
import { NavLink } from "react-router-dom";
// @material-ui/core components
import withStyles from "@material-ui/core/styles/withStyles";
import Drawer from "@material-ui/core/Drawer";
import Hidden from "@material-ui/core/Hidden";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Icon from "@material-ui/core/Icon";
// core components
import AdminNavbarLinks from "components/Navbars/AdminNavbarLinks.jsx";
import RTLNavbarLinks from "components/Navbars/RTLNavbarLinks.jsx";
import GridItem from "components/Grid/GridItem.jsx";
import GridContainer from "components/Grid/GridContainer.jsx";

import sidebarStyle from "assets/jss/material-dashboard-react/components/sidebarStyle.jsx";
import ListItemIcon from '@material-ui/core/ListItemIcon';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
import Context from "Context.js";
import Texty from 'rc-texty';
import 'rc-texty/assets/index.css';
const utils = require('../../utils/utils')

const styles ={
  nested: {
    paddingLeft: '500px',
  },
};

class Sidebar extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      childMenuOpen: false,
    };
  }

  onEnter() {
    return {
      enter: [
        {
          scale: 2,
          opacity: 0,
          type: 'set',
        },
        { scale: 1.2, opacity: 1, duration: 400 },
        { scale: 0.9, duration: 200 },
        { scale: 1.05, duration: 150 },
        { scale: 1, duration: 100 },
      ],
      leave: {
        opacity: 0, scale: 0,
      },
    };
  }

  handleChildMenu = (open) => {
    if (open) {
      this.setState({ childMenuOpen: !(this.state.childMenuOpen) });
    }
  };

  checkAccess (minRole) {
    console.log(this.props.userId)
  }

  // verifies if routeName is the one active (in browser input)
  activeRoute(routeName) {
    return this.props.location.pathname.indexOf(routeName) > -1 ? true : false;
  }
  render() {
    const { classes, color, logo, image, logoText, routes, userRole } = this.props;

  const links = (
    <List className={classes.list}>
      {routes.map((prop, key) => {
        var activePro = " ";
        var listItemClasses;
        // var access = utils.checkAccess(2)
        if (prop.path === "/upgrade-to-pro") {
          activePro = classes.activePro + " ";
          listItemClasses = classNames({
            [" " + classes[color]]: true
          });
        } else {
          listItemClasses = classNames({
            [" " + classes[color]]: this.activeRoute(prop.layout + prop.path)
          });
        }
        const whiteFontClasses = classNames({
          [" " + classes.whiteFont]: this.activeRoute(prop.layout + prop.path)
        });
        return (
          <NavLink
            to={prop.layout + prop.path}
            className={activePro + classes.item}
            activeClassName="active"
            key={key}
          >{
              utils.hasAccess(prop.minRole, userRole) &&
              <ListItem button className={classes.itemLink + listItemClasses} onClick={
                prop.open ? this.handleChildMenu : ''
                }>
                {typeof prop.icon === "string" ? (
                  <Icon
                    className={classNames(classes.itemIcon, whiteFontClasses, {
                      [classes.itemIconRTL]: this.props.rtlActive
                    })}
                    color="secondary"
                  >
                    {prop.icon}
                  </Icon>
                ) : (
                  <prop.icon
                    className={classNames(classes.itemIcon, whiteFontClasses, {
                      [classes.itemIconRTL]: this.props.rtlActive
                    })}
                  />
                )}
                <ListItemText
                  primary={
                    this.props.rtlActive ? prop.rtlName : prop.name
                  }
                  className={classNames(classes.itemText, whiteFontClasses, {
                    [classes.itemTextRTL]: this.props.rtlActive
                  })}
                  disableTypography={true}
                />
              </ListItem>
          }
            
            {/* <Collapse in={this.state.childMenuOpen} timeout="auto" unmountOnExit>
            {
              prop.open &&
              <div>
                {
                  prop.children.map((item, key) => 
                  <NavLink
                    to={item.layout + item.path}
                    className={activePro + classes.itemChild}
                    activeClassName="active"
                    key={key}
                  >
                  <ListItem button className={classes.itemLinkChild + listItemClasses}>
                  {typeof item.icon === "string" ? (
                    <Icon
                      className={classNames(classes.itemIconChild, whiteFontClasses, {
                        [classes.itemIconRTLChild]: this.props.rtlActive
                      })}
                    >
                      {item.icon}
                    </Icon>
                  ) : (
                    <item.icon
                      className={classNames(classes.itemIconChild, whiteFontClasses, {
                        [classes.itemIconRTLChild]: this.props.rtlActive
                      })}
                    />
                  )}
                  <ListItemText
                    primary={
                      this.props.rtlActive ? item.rtlName : item.name
                    }
                    className={classNames(classes.itemTextChild, whiteFontClasses, {
                      [classes.itemTextRTLChild]: this.props.rtlActive
                    })}
                    disableTypography={true}
                  />
                </ListItem>
                </NavLink>
                  )
                }
              </div>
            }
            </Collapse> */}
          </NavLink>
        );
      })}
    </List>
  );
  var brand = (
    <div className={classes.logo}>
      
      <GridContainer>
      <a
        href=""
        className={classNames(classes.logoLink, {
          [classes.logoLinkRTL]: this.props.rtlActive
        })}
      >
      <GridItem>
      <img src={logo} alt="logo" className={classes.img} />
      </GridItem>
      <GridItem><div className="texty-demo" style={{ align:'center', marginLeft: '50px' }}>
                    <Texty enter={this.onEnter}>{logoText}</Texty>
        </div></GridItem>
        </a>
      </GridContainer>
      
    </div>
  );
  return (
    <div>
      <Hidden mdUp implementation="css">
        <Drawer
          variant="temporary"
          anchor={this.props.rtlActive ? "left" : "right"}
          open={this.props.open}
          classes={{
            paper: classNames(classes.drawerPaper, {
              [classes.drawerPaperRTL]: this.props.rtlActive
            })
          }}
          onClose={this.props.handleDrawerToggle}
          ModalProps={{
            keepMounted: true // Better open performance on mobile.
          }}
        >
          {brand}
          <div className={classes.sidebarWrapper}>
            {this.props.rtlActive ? <RTLNavbarLinks /> : <AdminNavbarLinks />}
            {links}
          </div>
          {image !== undefined ? (
            <div
              className={classes.background}
              style={{ backgroundImage: "url(" + image + ")" }}
            />
          ) : null}
        </Drawer>
      </Hidden>
      <Hidden smDown implementation="css">
        <Drawer
          anchor={this.props.rtlActive ? "right" : "left"}
          variant="permanent"
          open
          classes={{
            paper: classNames(classes.drawerPaper, {
              [classes.drawerPaperRTL]: this.props.rtlActive
            })
          }}
        >
          {brand}
          <div className={classes.sidebarWrapper}>{links}</div>
          {image !== undefined ? (
            <div
              className={classes.background}
              style={{ backgroundImage: "url(" + image + ")" }}
            />
          ) : null}
        </Drawer>
      </Hidden>
    </div>
  );
  }
};

Sidebar.contextType = Context;

Sidebar.propTypes = {
  classes: PropTypes.object.isRequired
};

export default withStyles(sidebarStyle)(Sidebar);
