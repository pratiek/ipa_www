
var utils =  {
    hasAccess : (minRole, userRole) => {
        if(userRole >= minRole){
            return true;
        }
        return false;
    },
    rowPerPage : (rows) => {
        if(rows > 10) return 10;
        else return rows
    }
}

module.exports = utils