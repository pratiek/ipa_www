import React, { Component } from 'react';
import { Switch, Route, Redirect } from "react-router-dom";
import routes from "routes.js";
import LoginPage from "views/Login/Login.jsx";
import "assets/css/material-dashboard-react.css?v=1.6.0";

const switchRoutes = (
  <Switch>
    {routes.map((prop, key) => {
      if (prop.layout === "/login") {
        return (
          <Route
            path={prop.layout + prop.path}
            component={prop.component}
            key={key}
          />
        );
      }
    })}
  </Switch>
);

class Login extends Component {
  render() {
    const { classes, ...rest } = this.props;
    return (
      <div className="loginPage">
         <LoginPage />
      </div>
    );
  }
}

export default Login;
