

const axios = require('axios');
const queryString = require('query-string');
const remote = {
    address: window.app.env.API_URL
}

function getRequests(api) {
    const token = localStorage.getItem('token')
    const apiUrl = remote.address + api
    let promise = new Promise((resolve, reject) => {
        axios.get(apiUrl, { headers: 
                {
                    'Authorization' : `Bearer ${token}`
                } 
            })
        .then(res => {  
            var data = res.data
            resolve(data)
        })
        .catch(error => {
            var errorMessage = error.response.data.Message
            reject(errorMessage)
        })
    })
    return promise;
}

function postRequests(api, data) {
    const token = localStorage.getItem('token')
    const apiUrl = remote.address + api
    let promise = new Promise((resolve, reject) => {
        axios.post(apiUrl, data, { headers:
            {
                'Authorization' : `Bearer ${token}`
            } 
        })
        .then(res => {
            var data = res.data
            resolve(data)
        })
        .catch(error => {
            var errorMessage = error.response.data.Message
            reject(errorMessage)
        })
    })
    return promise;
}

var RemoteServices = {
    getUser: function () {
        const url = 'Users/GetUser'
        return getRequests(url)
    },
    sendLogin: function (loginData) {
        const url = 'Users/Login'
        return postRequests(url, loginData)
    },
    createUser : function (userData) {
        const url = 'Users/Signup'
        return postRequests(url, userData) 
    },
    createLocation : function (locationData) {
        const url = 'Storage/CreateLocations'
        return postRequests(url, locationData) 
    },
    getLocation: function(id) {
        const url = 'Storage/GetLocationRanges?LocationId='+id
        return getRequests(url)
    },
    getUsers : function () {
        const url = 'Users/UsersList'
        return getRequests(url)
    },
    deleteUser : function (userID) {
        const url = 'Users/DeleteUser'
        return postRequests(url, userID)
    },
    printBarCode: function (data) {
        const url = '/Barcode/Print'
        return postRequests(url, data)
    },
    storeBarCodeItems : function (data) {
        const url = 'Storage/StoreItems'
        return postRequests(url, data)
    },
    storeDispatchAdviceItems : function (data) {
        const url = '/Storage/ReleaseItems'
        return postRequests(url, data)
    },
    getPrintModes : function () {
        const url ='/Barcode/PrintConfig'
        return getRequests(url)
    },
    submitScannedBarCodes: function (scannedBarCodes) {
        const url = '/Barcode/ClassifyBarcodes'
        return postRequests(url, scannedBarCodes)
    },
    getGRN : function(){
        const url = 'GRN/Documents'
        return getRequests(url)
    },
    getGRNItems : function(documentIdentifier){
        const stringified = queryString.stringify(documentIdentifier);
        const url = 'GRN/DocumentItems?'+ stringified
        return getRequests(url)
    },
    getSalesReturn : function() {
        const url = 'SalesReturn/Documents'
        return getRequests(url)
    },
    getSalesReturnItems : function(documentIdentifier){
        const stringified = queryString.stringify(documentIdentifier);
        const url = 'SalesReturn/DocumentItems?'+ stringified
        return getRequests(url)
    },
    getPurchaseReturn : function() {
        const url = 'PurchaseReturn/Documents'
        return getRequests(url)
    },
    getPurchaseReturnItems : function(documentIdentifier){
        const stringified = queryString.stringify(documentIdentifier);
        const url = 'PurchaseReturn/DocumentItems?'+ stringified
        return getRequests(url)
    },
    getDispatchAdvice : function() {
        const url = 'DeliveryOrder/Documents'
        return getRequests(url)
    },
    getDispatchAdviceItems : function(documentIdentifier){
        const stringified = queryString.stringify(documentIdentifier);
        const url = 'DeliveryOrder/DocumentItems?'+ stringified
        return getRequests(url)
    },
    getInventoryTransferIn : function() {
        const url = 'InventoryTransferIn/Documents'
        return getRequests(url)
    },
    getInventoryTransferInItems : function(documentIdentifier){
        const stringified = queryString.stringify(documentIdentifier);
        const url = 'InventoryTransferIn/DocumentItems?'+ stringified
        return getRequests(url)
    },
    getInventoryTransferOut : function() {
        const url = 'InventoryTransferOut/Documents'
        return getRequests(url)
    },
    getInventoryTransferOutItems : function(documentIdentifier){
        const stringified = queryString.stringify(documentIdentifier);
        const url = 'InventoryTransferOut/DocumentItems?'+ stringified
        return getRequests(url)
    },
    getItemLocations :  function(items){
        //const stringified = queryString.stringify(documentIdentifier);
        const url = '/Storage/FindItems';
        return postRequests(url, { ItemsToFind : items })
    },
    getCompanyList : function() {
        const url = 'BranchList/CompanyList'
        return getRequests(url)
    },
    getBranchList : function(companyCode) {
        const url = 'BranchList/BranchList'+'?companyCode='+companyCode
        return getRequests(url, companyCode)
    },
    getBarcodeList : function(itemCode) {
        const url = 'Barcode/PrintedBarcodeList'+'?itemCode='+itemCode
        return getRequests(url)
    },
    getDashboardStats : function() {
        const url = 'Dashboard/DashboardStats'
        return getRequests(url)
    },
    getLocationStatusReport : function(id,itemCode) {
        const url = 'Report/locationStatusReport?ERPLocationId='+id+'&itemCode='+itemCode
        return getRequests(url)
    },
    getOpeningStockTemplate : function() {
        const url = 'OpeningStock/OpeningStockTemplate'
        return getRequests(url)
    },
    getOpeningStockItemStatus : function(docIdentifier, itemCode) {
        const stringified = queryString.stringify(docIdentifier);
        const url = 'OpeningStock/getItemStatus?'+ stringified + '&itemCode='+itemCode
        return getRequests(url)
    },
    getERPLocations: function () {
        const url = 'Storage/ERPLocations'
        return getRequests(url)
    },
    setStoredERPLocations: function (locationIdentifier) {
        const url = 'Storage/AddERPLocation'
        return postRequests(url, locationIdentifier)
    },
    getStoredERPLocations: function () {
        const url = 'Storage/getERPLocations'
        return getRequests(url)
    },
    getAllLocations: function (locationId, block, side) {
        const url = 'Storage/GetAllLocations?LocationId='+ locationId +'&Block='+block+'&Side='+side 
        return getRequests(url)
    },
    printLocations : function(locParams){
            const url = "Storage/PrintLocations?locParams="+JSON.stringify(locParams);
            return getRequests(url)
    },
    updateUser : function(user) {
        const url = 'Users/UpdateUser'
        return postRequests(url, user)
    },
    updateUserPassword: function(userDetails) {
        const url = 'Users/UpdatePassword'
        return postRequests(url, userDetails)
    },
    getBarcodeHTML : function(instanceId) {
        const url = '/Barcode/GetBarcodeHTML?instanceId=' + instanceId
        return getRequests(url)
    },
    getStoredBarcodesList : function(locId,itemCode) {
        const url = 'Report/GetStoredBarcodes?ERPLocationId=' + locId +'&itemCode='+itemCode
        return getRequests(url)
    },
    getItemsFromErpLocation: function (locId) {
        const url = 'Report/GetItemsFromErpLocation?ERPLocationId=' + locId
        return getRequests(url)
    },
    getAgeingReport: function(locId,itemCode) {
        const url = 'Report/GetAgeingReport?ERPLocationId=' + locId +'&itemCode='+itemCode
        return getRequests(url)
    },
    getStoredBarcodesListForAgeing: function(locId,itemCode,storageDate) {
        const url = 'Report/GetBarCodeForAgeing?ERPLocationId=' + locId +'&itemCode='+ itemCode +'&storageDate='+storageDate
        return getRequests(url)
    }
}

module.exports = RemoteServices;